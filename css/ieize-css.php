<?php
/* Kudos to tesla@och.nu for most of the script.
   What this script does is to remove all media query code. It leaves all declarations inside said media queries intact, but since parsers 
   default to the last declaration seen, the result is declarations in the last media query in the input file will be used. (This file will 
   normally only be used for IE<9, and at worst we might see 800x600 (most likely 1024x768 though). At least, we're unlikely to see any 
   mobile browsers. Thus, old IEs will get a BEARABLE, if not ideal, version of the design.) */

if (!isset($_GET['d']) || $_GET['d']=="")
	$cssfile = 'style.css'; /* Making an "educated" guess */
else
	$cssfile = $_GET['d'];

$data=file_get_contents($cssfile);
$data=preg_replace('#/\*(?:(?!\*/).)*\*/#','',$data);	/* Strip comments in case they contain css */
$out='';

$level=0;
for($i=0;$i<strlen($data);$i++) {
	switch($data[$i]) {
	case "\r":
	case "\n":
	break;
	case '@':
		if(strpos($data,'@media',$i)==$i) {
			while($data[++$i]!='{');	/* Erase to first '{' */
		}
	break;
	case '{':
		$level++;
		$out.='{';
	break;
	case '}':
		$level--;
		if($level>=0) {
			$out.='}';
		} else {
			$level=0;	/* Erase unmatched '}' which is assumed to be from an erased media-query. */
		}
	break;
	default:
		$out.=$data[$i];	/* This looks slow but it is not so bad in reality */
	}
}

header("Content-Type: text/css; charset=utf-8");
header("Content-Length: ".strlen($out));
echo $out;

?>
