(function($){
	$(document).ready(function(){

		// ##### Initialize grid #####
		$('#boxes').removeClass('nojs').masonry({
			itemSelector : 'article',
			columnWidth : '.grid-sizer',
			percentPosition : true,
			gutter : 20,
			fitWidth : false,
			stagger : 0,
			resize : true, // Default
			transitionDuration : 0,
		});


		// ##### Initialize banner slideshow #####
		$('#banner-slideshow').show().find('li:gt(0)').hide();
		$('#banner').css('background-image', '');
		setInterval(function() { 
			$('#banner-slideshow > li:first')
				.fadeOut(1500)
				.next()
				.fadeIn(1500)
				.end()
				.appendTo('#banner-slideshow');
		},  5000);


		// ##### Menus #####
		$('#headerbar nav > ul').hide(); // Allows menu to be always visible for no-js browsers
		$('#headerbar-menu').show();

		$('#headerbar nav').on('click', function(ev){
			if( ! $(ev.target).is('.tip-bubble') )
				$(this).find('>ul').slideToggle('fast');
		}).on('mouseenter', function(ev){
			if( ! $(ev.target).is('.tip-bubble') )
				$(this).find('.tip-bubble').fadeOut(900);
		});

		$('#headerbar nav .tip-bubble').show();
		window.setTimeout(function(){
			$('#headerbar nav .tip-bubble').fadeOut(2500);
		}, 16000);

		$('#headerbar nav .tip-bubble').on('mouseenter', function(){
			$(this).fadeIn('fast');
		}).on('click', function(){
			$(this).fadeOut(900);
		});


		// ##### Various closing behaviours #####
		// (i.e. doing-something-when-clicking-outside-an-element stuff)
		$('body').on('click', function(ev){
			if( ! $(ev.target).parents('#headerbar-menu').length && ! $(ev.target).is('#headerbar-menu') )
				$('#headerbar-menu > ul').slideUp('fast');
			if( ! $(ev.target).parents('#headerbar-languages').length && ! $(ev.target).is('#headerbar-languages') )
				$('#headerbar-languages > ul').slideUp('fast');

			if($('#modal-window').is(':visible') && ! $(ev.target).parents('#modal-window').length && ! $(ev.target).is('#modal-window') )
				window.location.hash = '';

			//console.log(ev.target);
		});


		// ##### Timeline(s) #####
		// Run this function on a table which has the needed data attributes etc to be a timeline.
		// In particular, tr:s need a data-start attribute with a timestamp as value.
		$.fn.timelineInit = function(start, end) {
			$(this).each(function(){
				var $this = $(this),
					$line,
					timeline_start, 
					timeline_end, 
					span, 
					span_in_days;

				// Set up defaults
				if(typeof start === 'undefined' && $this.data('timeline-start') != '')
					timeline_start = $this.data('timeline-start');
				else if(typeof start === 'undefined')
					timeline_start = $this.find('tr').first().data('start');
				else
					timeline_start = start;

				if(typeof end === 'undefined' && $this.data('timeline-end') != ''){
					timeline_end = $this.data('timeline-end');
				}
				else if(typeof end === 'undefined'){
					var lasttr = $this.find('tr').last();

					if(lasttr.data('end') != '')
						timeline_end = lasttr.data('end');
					else
						timeline_end = lasttr.data('start');
				}
				else {
					timeline_end = end;
				}

				span = timeline_end - timeline_start;
				span_in_days = Math.ceil(span/86400);

				// Add parsed class to table and create UI elements
				$this.addClass('js-parsed').wrap('<div class="timeline-table-wrapper"></div>');
				$this.closest('.timeline-table-wrapper').append('<a class="tl-left" href="javascript:void(0)" title="Scroll left"></a><a class="tl-right" href="javascript:void(0)" title="Scroll right"></a>');
				$this.find('td').prepend('<h3></h3>');
				$this.find('td > h3').append('<a class="prev fa fa-step-backward" href="javascript:void(0)" title="Prev"></a><a class="next fa fa-step-forward" href="javascript:void(0)" title="Next"></a><a class="close fa fa-times" href="javascript:void(0)" title="Close"></a>');

				// Draw actual timeline
				$this.prepend('<div class="timeline-line"></div>');
				$line = $this.find('.timeline-line');
				$this.add($line).css({
					'width'  : 2*span_in_days + 'px',
					'height' : 2*span_in_days + 'px'
				}); // 2 is a magic number.

				// Handle maximum scroll
				$this.setTimelineScrollData();

				// Add marks for months, years etc ...
				var walking_date, print_date, date_percent, months = langStrings.months;

				// Year
				walking_date = new Date(timeline_start * 1000);
				while (walking_date.getTime() < timeline_end * 1000) {
					print_date = walking_date.getUTCFullYear();
					date_percent = 100 * (walking_date.getTime() / 1000 - timeline_start)/span;

					if(date_percent < 0)
						date_percent = 0;

					$line.append('<div class="year-mark" style="left:' + date_percent + '%;top:' + date_percent + '%">' + print_date + '</div>');
					walking_date.setUTCFullYear(print_date + 1, 0, 1);
				}
				$line.append('<div class="year-mark last-year-mark"></div>'); // Add an extra line at the end just for looks

				// Month
				walking_date = new Date(timeline_start * 1000);
				while (walking_date.getTime() < timeline_end * 1000) {
					print_date = walking_date.getUTCMonth();
					date_percent = 100 * (walking_date.getTime() / 1000 - timeline_start)/span;

					if(date_percent < 0)
						date_percent = 0;

					$line.append('<div class="month-mark" style="left:' + date_percent + '%;top:' + date_percent + '%"><span>' + months[print_date] + '</span></div>');
					walking_date.setUTCMonth(print_date + 1, 1); // setMonth converts values outside the normal range (eg month=15, day=32) nicely
				}

				// PLACE MARKERS AND BOXES on the timeline
				$this.find('tr').each(function(i){
					var $row = $(this), 
						start = $row.data('start'), 
						end = $row.data('end'), 
						span_to_this = start - timeline_start, 
						length,
						percent;

					if (i>0)
						$row.addClass('closed');

					if(end == '')
						end = start;

					// If the start date of this tr is above timeline_end or end date before timeline_start, then hide this tr
					if(start > timeline_end || end < timeline_start){
						$row.hide();
						return;
					}

					// If end date of this tr is above timeline_end, then "shorten" this tr (NB hidden trs stay hidden!)
					if(end > timeline_end)
						end = timeline_end;

					// If start date of this tr is below timeline_start, then "shorten" this tr (NB hidden trs stay hidden!)
					if(start < timeline_start)
						start = timeline_start;

					// Print date in header
					$row.find('td>h3').prepend($row.find('th').html());

					// Figure out percent position
					percent = 100 * span_to_this/span;
					$row.css({
						'left': percent + '%',
						'top' : percent + '%'
					});

					// Figure out length (= size) of marker
					var time_length = (end - start)/span; // Fraction of line's "time length"
					var length = time_length * (2*span_in_days); // Pixel length
					length = 100 * length / $row.width(); // Fraction of tr's width
					
					var height = time_length * (2*span_in_days); // Pixel HEIGHT ...
					height = 100 * height / $row.height();
					$row.find('th').css({
						'width'  : length + '%',
						'height' : height + '%'
					});

					// If bubble box falls outside the timeline width/height, move box sufficiently to the left/top
					var right_check = ($row.offset().left + $row.find('td').width()) - ($line.offset().left + $line.width()), 
						bottom_check = ($row.offset().top + $row.find('td').height()) - ($line.offset().top + $line.height()); 

					if (right_check > 0 && $line.width() > 50) {
						$row.find('td').addClass('offset-right');
					}
					if (bottom_check > 0 && $line.width() <= 50) {
						$row.find('td').addClass('offset-bottom');
					}
				});

				// Horizontal scroll
				$this.siblings('.tl-right').on('click', function(){  // test - should be some buttons or something...
					$this.scrollTimeline('right');
				});
				$this.siblings('.tl-left').on('click', function(){  // test - should be some buttons or something...
					$this.scrollTimeline('left');
				});
				$this.on('wheel', function(ev){
					if( $this.data('scrolling') != 'true' ){
						if( ev.ctrlKey == false && ! $this.find('td').has($(ev.target)).length ) {
							ev.preventDefault();
							$this.data('scrolling', 'true');

							if (ev.originalEvent.deltaY < 0) // up
								$this.scrollTimeline('left', function(){ $this.data('scrolling', 'false'); });
							else if (ev.originalEvent.deltaY > 0) // down
								$this.scrollTimeline('right', function(){ $this.data('scrolling', 'false'); });
							else
								$this.data('scrolling', 'false');
						}
						else if(ev.ctrlKey == true){
							// TODO: ZOOM 
							// Should really just change length of .timeline-line and recalculate max-scroll. 
							// Maybe keep a record of zoom level and have some limits.
							// Nice-to-have: Keep current middle of "window" centered


						}
					}
				});

				// Click events
				$this.find('.close').on('click', function(){
					$(this).closest('tr').addClass('closed');
				});
				$this.find('th').on('click', function(){
					$(this).closest('tr').toggleClass('closed');
				});
				$this.find('tr').on('click', function(){
					$(this).siblings('tr').css('z-index', 1);
					$(this).css('z-index', 2);
				});
				$this.find('.prev').on('click', function(){
					$(this).closest('tr').addClass('closed').prev('tr').removeClass('closed');
				});
				$this.find('.next').on('click', function(){
					$(this).closest('tr').addClass('closed').next('tr').removeClass('closed');
				});
			});
		}

		$.fn.setTimelineScrollData = function(){
			if($(this).is('.timeline-table:visible')){
				var ttww = $(this).closest('.timeline-table-wrapper').width(),
					ms = ($(this).find('.timeline-line').width() - ttww),
					fifty = ttww / 2;

				$(this).data('max-scroll', Math.ceil(ms));
				$(this).data('scroll-step', Math.round(fifty));

				var scroll = parseInt($(this).css('left')) * -1;
				if(scroll > ms){
					$(this).css(
						{ left : '-' + ms + 'px' }
					);
				}
			}
		}

		$.fn.scrollTimeline = function(direction, finishCb, step){
			if($(this).is('.timeline-table:visible')){
				if(typeof direction === 'undefined' || direction != 'left' )
					direction = 'right';

				if (typeof step === 'undefined')
					step = parseInt($(this).data('scroll-step'));

				var maxScroll = Math.ceil(parseFloat($(this).data('max-scroll'))),
					scrollNow = Math.abs(parseInt($(this).css('left'))),
					scrollTo;

				if(direction == 'right')
					scrollTo = scrollNow + step;
				else
					scrollTo = scrollNow - step;

				if (scrollTo > maxScroll)
					scrollTo = maxScroll;

				if (scrollTo < 0)
					scrollTo = 0;

				$(this).animate(
					{ left : '-' + scrollTo + 'px' },
					600,
					function(){
						if(typeof finishCb === 'function')
							finishCb.call();
					}
				);
			}
		}

		$.fn.zoomTimeline = function(direction, finishCb){
			if($(this).is('.timeline-table:visible')){
				if(typeof direction === 'undefined' || direction != 'in' )
					direction = 'out';


			}
		}

		if($('.timeline-table:visible').length)
			$('.timeline-table:visible').timelineInit();

		$(window).on('resize', function(){
			$('.timeline-table:visible').setTimelineScrollData();

			// If bubble box falls outside the timeline width/height, move box sufficiently to the left/top
			// We check our screen vs the breakpoint by considering .timeline-line's width, which the stylesheet determines
			$('.timeline-table:visible').find('tr').each(function(){
				var $row = $(this),
					$line = $row.closest('.timeline-table').find('.timeline-line'),
					right_check = ($row.offset().left + $row.find('td').width()) - ($line.offset().left + $line.width()),
					bottom_check = ($row.offset().top + $row.find('td').height()) - ($line.offset().top + $line.height()); 

				$row.find('td').removeClass('offset-right offset-bottom');

				if (right_check > 0 && $line.width() > 50) {
					$row.find('td').addClass('offset-right');
				}
				if (bottom_check > 0 && $line.width() <= 50) {
					$row.find('td').addClass('offset-bottom');
				}
			});
		});



		// ##### Popup/modal #####
		function modalContentHeight(){
			var $popup = $('#modal-window');
			if($popup.is(':visible')){
				$popup.find('.modal-content').css('height', $popup.height() - $popup.find('header').outerHeight() + 'px');
			}
		}

		function openModal(box, backClose) {
			var $this = $(box);

			if (! $this.length){
				return false; 
			}

			if(typeof backClose === 'undefined')
				var backClose = false;

			var headerText = $this.find('header').html();
				content = $this.contents().not('header').not('.view-more').clone(),
				id = $this.attr('id'),
				$popup = $('#modal-window');

			$popup.find('header h2').remove();
			$popup.find('header').append(headerText);
			$popup.find('.modal-content').empty();
			$popup.find('.modal-content').append(content);

			if (!backClose){
				$popup.find('.close').removeClass('backClose');
			}

			$popup.show();
			modalContentHeight();

			if($popup.find('.timeline-table').length)
				$popup.find('.timeline-table').timelineInit();

			return true;
		}

		$(window).on('resize', function(){
			modalContentHeight();
		});

		// On load as well
		modalContentHeight();

		// Closing popup
		$('#modal-window .close').on('click', function(){
			window.location.hash = '';
		});
		$(document).keyup(function(ev) {
			if (ev.keyCode == 27 && $('#modal-window').is(':visible')) { // 27=Esc
				window.location.hash = '';
			}
		});

		// Opening popup
		$('#boxes article').on('click', function(ev){
			ev.preventDefault();
			window.location.hash = '#!' + $(this).attr('id');
		});


		// ##### Hash handlers #####
		function hashCheck(onLoad){
			if(typeof onLoad === 'undefined')
				var onLoad = false;

			var hash = window.location.hash,
				firstChar = hash.substring(0,1),
				firstTwoChars = hash.substring(0,2);

			if(hash == '#' || hash == ''){
				$('#modal-window').hide();
				$('#boxes article').removeClass('active');
			}
			else if(firstTwoChars != '#!' && firstChar != '!'){
				$('#modal-window').hide();
				$('#boxes article').removeClass('active');
				$(window.location.hash).addClass('active');
			}
			else if (firstChar == '!') {
				var boxId = hash.substring(1);
				if (false == openModal('#' + boxId, !onLoad)){
					window.location.hash = '';
				}
			}
			else {
				var boxId = hash.substring(2);
				if(false == openModal('#' + boxId, !onLoad)){
					window.location.hash = '';
				}
			}
		}

		$(window).on('hashchange', function(){
			hashCheck();
		});
		hashCheck(true); // On load as well


		// ##### Close main confirm message #####
		$('#mailconfirm .close').on('click', function(){
			$('#mailconfirm').fadeOut('fast');
		});

	});
})(jQuery);