<?php
	// VARIABLES
	define('CONTACT_EMAIL', 'erika@erikanilsson.eu');
	define('SITE_TITLE', "<strong>Emil</strong> <span class='small-hide'>Kári</span> Petersson");
	
	function banner_image($num) { return "img/banner-$num.jpg"; }
	$banner_image = banner_image(rand(1,5)); // This will be the fallback, to show in background if slideshow breaks.
	$auto_selected_language = false;
	$language = "sv_SE";
	$langs = array(
		'Svenska' => 'sv_SE', 
		'English' => 'en_GB', 
		//'Íslenska' => 'is_IS',
	);

	// Handle contact form messages
	if (isset($_POST['contact_form_msg']) && !empty(trim($_POST['contact_form_msg'])) && isset($_POST['contact_adr']) && filter_var($_POST['contact_adr'], FILTER_VALIDATE_EMAIL)){
		$subject = isset($_POST['sbj']) ? $_POST['sbj'] : '';
		$subject = '[#HittaEmil kontaktformulär] ' . strip_tags($subject);
		$message = strip_tags($_POST['contact_form_msg']);
		$from = filter_var($_POST['contact_adr'], FILTER_SANITIZE_EMAIL);

		$message = "Avsändar-IP: " . $_SERVER['REMOTE_ADDR'] . "\r\n"
				 . "User-Agent: " . $_SERVER['HTTP_USER_AGENT'] . "\r\n\r\n\r\n"
				 . $message;

		$headers = 'From: ' . $from . "\r\n" .
			'Reply-To: ' . $from . "\r\n" .
			'X-Mailer: PHP/' . phpversion();

		if (mail(CONTACT_EMAIL, $subject, $message, $headers)) {
			define('MAIL_SENT', true);
		}
	}

	/**
	 * http://stackoverflow.com/a/11161193
	 * Arrayifies and sorts HTTP_ACCEPT_LANGUAGE by priority and specificity. 
	 * Returns an array in the form $language_code => $priority.
	 */
	function Get_Client_Prefered_Language ($getSortedList = false, $acceptedLanguages = false)
	{
		if (empty($acceptedLanguages))
			$acceptedLanguages = $_SERVER["HTTP_ACCEPT_LANGUAGE"];

		// regex inspired from @GabrielAnderson on http://stackoverflow.com/questions/6038236/http-accept-language
		preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})*)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $acceptedLanguages, $lang_parse);
		$langs = $lang_parse[1];
		$ranks = $lang_parse[4];

		$lang2pref = array();
		for($i=0; $i<count($langs); $i++)
			$lang2pref[$langs[$i]] = (float) (!empty($ranks[$i]) ? $ranks[$i] : 1);

		$cmpLangs = function ($a, $b) use ($lang2pref) {
			if ($lang2pref[$a] > $lang2pref[$b])
				return -1;
			elseif ($lang2pref[$a] < $lang2pref[$b])
				return 1;
			elseif (strlen($a) > strlen($b))
				return -1;
			elseif (strlen($a) < strlen($b))
				return 1;
			else
				return 0;
		};

		uksort($lang2pref, $cmpLangs);

		if ($getSortedList)
			return $lang2pref;

		reset($lang2pref);
		return key($lang2pref);
	} // END Get_Client_Prefered_Language

	// Determine default language, from GET variable or Accept-Language header
	if (isset($_GET['lang']) && in_array($_GET['lang'], $langs)) {
		$language = $_GET['lang'];
	}
	else if ($_SERVER['HTTP_ACCEPT_LANGUAGE'] != '*' ) { // * means "any" so Swedish is OK
		$accepted = Get_Client_Prefered_Language(true);
		foreach ($accepted as $l => $prio) {
			$short = preg_quote(explode('-', $l)[0], '/'); // Pick out only the language part
			$regex = '/^' . $short . '_[a-zA-Z]{2}$/';
			$grepd = preg_grep($regex, $langs);

			if($grepd){
				$language = reset($grepd);
				break;
			}
		}

		$auto_selected_language = true;
	}
	else {
		$auto_selected_language = true; // Language IS automatically chosen (it's the default)
	}

	// Get language file for the language selected above
	if(file_exists("lang/$language.json")) {
		$data = file_get_contents("lang/$language.json");
	}
	elseif(file_exists("lang/en_GB.json")) {
		$data = file_get_contents("lang/en_GB.json");
	}
	else { // So FYI we must make sure there's ONE language in there
		$files = glob("lang/*_*.json");
		$data = file_get_contents($files[0]);
	}

	$data = json_decode($data);

	function getboxes($dir) {
		$files = glob("{$dir}/*.md");
		natsort($files);
		$boxes = array();

		foreach ($files as $filename) {
			$boxes[str_replace('.md', '', explode('/', $filename)[2])] = file_get_contents($filename);
		}

		return $boxes;
	}

	// Get box data
	// TODO: We could conceivably put this in a function.
	$boxdirs = glob("boxes/*_*", GLOB_ONLYDIR);
	if (in_array("boxes/{$language}", $boxdirs)) {
		$boxes = getboxes("boxes/{$language}");
		$boxdir = "boxes/{$language}";
	}
	elseif (in_array("boxes/en_GB", $boxdirs)) { // Fallback: Use English
		$boxes = getboxes("boxes/en_GB");
		$boxdir = "boxes/en_GB";
	}
	elseif (count($boxdirs) > 0) { // "Desperation fallback": Pick the first language we find!
		$d = $boxdirs[0];
		$boxes = getboxes($d);
		$boxdir = $d;
	}
	else { // There are no languages, sorry.
		$boxes = null;
		$boxdir = null;
	}
	unset($boxdirs);
?>
<!DOCTYPE html>
<html lang="<?php echo $language ?>">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1" />

	<title>#FindEmil - <?php echo strip_tags(SITE_TITLE) ?></title>

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
	<!--[if lt IE 9]>
	<script type="text/javascript">
		//Teaches oldish IEs to believe these elements exist
		document.createElement('header');
		document.createElement('nav');
		document.createElement('section');
		document.createElement('article');
		document.createElement('aside');
		document.createElement('footer');
	</script>
	<link rel="stylesheet" type="text/css" href="css/ieize-css.php?d=style.css" />
	<![endif]-->

	<!--[if lte IE 7]>
	<style type="text/css">
		/* IE<8 don't understand box-sizing:border-box, so they'll add padding to the 'outside' of elements anyway. Thus, recalculate some widths. */
		#modal-content {
			// Have a look at the styling for this
		}
	</style>
	<![endif]-->

	<meta name="description" content="<?php echo $data->siteDescription ?>">
	<meta property="og:title" content="#FindEmil - <?php echo strip_tags(SITE_TITLE) ?>">
	<meta property="og:site_name" content="#FindEmil - <?php echo strip_tags(SITE_TITLE) ?>">
	<meta property="og:type" content="website">
	<meta property="og:locale" content="<?php echo $language ?>">
	<?php foreach($langs as $l => $c) : ?>
		<meta property="og:locale:alternate" content="<?php echo $c ?>">
	<?php endforeach; ?>
	<meta property="og:image" content="http://erikanilsson.eu/findemil/img/og/emil1.jpg">
	<meta property="og:image" content="http://erikanilsson.eu/findemil/img/og/emil2.jpg">
	<meta property="og:image" content="http://erikanilsson.eu/findemil/img/og/emil3.jpg">
	<meta property="og:description" content="<?php echo $data->siteDescription ?>">

	<meta name="twitter:card" content="summary">
	<meta name="twitter:title" content="#FindEmil - <?php echo strip_tags(SITE_TITLE) ?>">
	<meta name="twitter:image" content="http://erikanilsson.eu/findemil/og/emil1.jpg">
	<meta name="twitter:description" content="<?php echo $data->siteDescription ?>">

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-5295664-2', 'auto');
		ga('send', 'pageview');
	</script>

</head>
<body>
<?php if(defined('MAIL_SENT') && MAIL_SENT === true) : ?><div id="mailconfirm"><?php echo $data->mailconfirm ?><a href="javascript:void(0)" class="close fa fa-times"></a></div><?php endif; ?>

<header id="headerbar">
	<h1 id="headerbar-title"><a href="#" class="white" style="border-bottom:0 none !important"><?php echo SITE_TITLE; ?></a></h1>

	<nav id="headerbar-menu">
		<ul>
			<?php 
				if(isset($boxes)) {
					foreach ($boxes as $filename => $box) {
						// Find header
						$header = $filename;
						if (false !== preg_match('/<header><h2(.*?)>(.*?)<\/h2><\/header>/i', $box, $matches)) {
							$header = $matches[2];
						}


						echo "<li><a href='#{$filename}'>$header</a></li>\r\n";
					}
				}
			?>
		</ul>

		<div class="tip-bubble bubble-left">
			<?php echo $data->quicklinksInfo ?>
		</div>
	</nav>

	<nav id="headerbar-languages" class="lang-sv">
		<ul>
			<?php
				foreach ($langs as $lang => $code){
					echo "<li><a href='?lang=$code'>$lang</a></li>\r\n";
				}
			?>
		</ul>

		<div class="tip-bubble bubble-right">
			<?php echo $data->languagesInfo ?>
		</div>
	</nav>
</header>

<header id="banner" style="background-image:url(<?php echo $banner_image ?>)">
	<ul id="banner-slideshow">
	<?php
		for($i=1; $i<=6; $i++) :
			echo "<li style='background-image:url(" . banner_image($i) . ")'></li>";
		endfor;
	?>
	</ul>
	<ul id="banner-list">
		<?php
			foreach ($data->bannerList as $listItem) :
				echo "<li class='{$listItem->classes}'>";
				echo $listItem->text;
				echo "</li>";
			endforeach;
		?>
	</ul>
</header>

<!-- Boxes here -->
<section id="boxes" class="nojs">
	<article class="grid-sizer" style="border:0; padding:0; margin:0"></article>

	<?php 
		if (isset($boxes)) {
			require_once("Parsedown.php");
			require_once("ParsedownExtra.php");
			$MD = new ParsedownExtra();

			foreach ($boxes as $filename => $box) {
				$class = "";

				// Find box width, if present
				if (false !== preg_match('/^<!--( )*\[cols=(\d)\]( )*-->/i', $box, $matches)) {
					$class .= 'col-' . $matches[2];
					$box = str_replace($matches[0], '', $box);
				}

				// Parse markdown in box content
				$html = $MD->text($box);

				// Find break, if present
				$split = explode('<hr />', $html, 2);
				if(count($split) > 1)
					$html = $split[0] . '<div class="fullcontent">' . $split[1] . '</div>';
				else
					$html = $split[0];

				// Check for "timeline" tag if present
				if (false !== preg_match('/<!--( )*\[timeline\]( )*-->/i', $html, $matches)) {
					$timeline = "";
					$json = file_get_contents("$boxdir/timeline.json");
					$json = $json ? json_decode($json) : null;

					if($json->events) : 
						$timeline .= "<table class='timeline-table' data-timeline-start='{$json->timelineStart}' data-timeline-end='{$json->timelineEnd}'><tbody>\r\n";
						foreach ($json->events as $event) : 
							$event_class = $event->topic ? "timeline-topic-{$event->topic} timeline-topic" : "";
							$timeline .= "<tr data-start='{$event->start}' data-end='{$event->end}' class='$event_class'>";
							$timeline .= "<th>{$event->dateString}</th>";
							$timeline .= "<td>" . $MD->text($event->text) . "</td></tr>\r\n";
						endforeach;
						$timeline .= "</tbody></table>";
					endif;

					$html = str_replace($matches[0], $timeline, $html);
				}

				// Print
				echo "<article id='$filename' class='$class'>";
				echo $html;
				echo "<a class='view-more' href='nojs.php?lang=$language&page=$filename'>{$data->viewMore}</a></article>\r\n"; // TODO make this page
			}
		}
	?>
</section>

<section id="modal-window">
	<header><div class="close black fa fa-times"> </div></header>
	<div class="modal-content"></div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>

<script type="text/javascript">
	var langStrings = {
		months: <?php echo json_encode($data->timelineMonths); ?>,
	};
</script>

<script src="js/scripts.js"></script>
</body>
</html>