* [x] Dokumentation
* [x] Sammanfattning
* [x] Timeline
* [x] Facebookposterna
* [ ] Press/nyheter/rapportering
* [ ] Hjälp oss
* [-] Om egenmäktighet/barnabortföranden ((finns kortfattat under "dokument"))
* [ ] Beskrivningar
* [x] Om CPT1a
* [x] Vad detta INTE är (vårdnadstvist, stalking osv) alt vanliga frågor
* [x] "Om oss" <-- Peter & Erika

Kryssade punkter är klara på sv och en. Slashade punkter klara på ett språk (oftast svenska)



-------------

{
	"text" : "Magðalena har sålt sin lägenhet, sagt upp sig och angivit sin mors adress på Island som särskild postadress. Isländska släktingar har antingen sagt sig inget veta, eller aktivt undvikit polis.",
	"classes" : ""
},
{
	"text" : "Emils svenska släkt har inte sett eller hört av honom sedan bortförandet eller fått någon kontakt med Magðalena.",
	"classes" : ""
},

-------------

{
	"text" : "Magðalena has sold her flat, quit work and given authorities her mother's Iceland address as a forwarding address. Icelandic relatives have either said they're not involved, or actively avoided police.",
	"classes" : ""
},
{
	"text" : "Emil's Swedish family have neither seen nor heard from him since the abduction nor received any contact from Magðalena.",
	"classes" : ""
}