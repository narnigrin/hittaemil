<!--[cols=2]-->
<header><h2 class="red">Belägg och dokumentation</h2></header>

Är du skeptisk? **Bra!** Kritiskt tänkande är både rimligt och nödvändigt. 

Här finns en samling av de flesta dokument och länkar som är relevanta. Vill du själv kontrollera fakta, kan du t.ex. beställa domstolshandlingar direkt från domstolen genom att ringa eller maila. Skaraborgs tingsrätt, som handlagt de flesta målen, har kontaktuppgifter på [skaraborgstingsratt.domstol.se](http://www.skaraborgstingsratt.domstol.se/). Viss övrig info går också att hitta i öppna register, till exempel folkbokföringen.

----------------

### Häktningsbeslutet {.violet}

**Magðalena Níelsdóttir häktades den 31 mars 2016 i sin frånvaro för grov egenmäktighet med barn.**

* [Protokoll från häktningsförhandling i Skaraborgs tingsrätt, mål B1093-16](sources/B1093-16.pdf). Personnummer och vissa andra uppgifter bortredigerade.
* [Protokoll ang. omhäktning (dvs. fortsatt häktning) 8 juni 2017](sources/B1093-16-omhaktning.pdf) (mål B1093-16). Personnummer och vissa andra uppgifter bortredigerade.
* [Notis om häktningen](http://www.falkopingstidning.se/article/mamma-haktad-i-vardnadstvist/) i Falköpings tidning den 31 mars 2016.

Brottsrubriceringen ["egenmäktighet med barn" (BrB 7 kap 4 §)](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/brottsbalk-1962700_sfs-1962-700#K7P4) är applicerbar när en person som _inte ensam_ har vårdnaden om ett barn utan god orsak skiljer barnet från dess vårdnadshavare utan den vårdnadshavarens tillstånd, alternativt behåller barnet hos sig utan vårdnadshavarens tillstånd. Det kan alltså begås av en person som inte har del i vårdnaden om barnet, eller en person som har del i vårdnaden; och personen kan ha haft rätt att ta barnet från (den andra) vårdnadshavaren men behållit det hos sig för länge, till exempel vägrat lämna över barnet för umgänge. Brottet räknas som grovt om barnet har förts eller hållits kvar utomlands, om kvarhållandet har skett under lång tid, eller om den som fört bort barnet visat tecken på att inte planera att återvända (till exempel). Straffskalan för grov egenmäktighet med barn är fängelse från 6 månader till 4 år.

### Vårdnaden {.green}

**Vårdnaden om Emil Petersson anförtroddes den 11 maj 2016 hans far, Peter Bengtsson, ensam.** Socialtjänsten förordade beslutet. Dessförinnan hade Emils föräldrar gemensam vårdnad om honom och hans boende var hos hans mor, Magðalena Níelsdóttir.

* [Protokoll från huvudförhandling i mål T3338-14](sources/Protokoll-domstol-20160511.pdf), Skaraborgs tingsrätt. Personnummer och vissa andra personuppgifter bortredigerade.
<!--* Utdrag ur personregistret för Emil Petersson, daterat XXXXXXXX. Vissa personuppgifter bortredigerade.-->
* [Utdrag ur Socialnämnden i Falköpings utredning om vårdnad, boende och umgänge](sources/Utdrag-Soc.pdf), inlämnad till tingsrätten den 20 augusti 2015. (Filen innehåller utdrag ur flera utredningar.)

### Emils åkomma {.orange}

* (Del av) [akutbrev från Emils läkare](sources/akutblad-s1.pdf) vid Drottning Silvias barn- och ungdomssjukhus (DSBUS) med information om hans åkomma CPT 1A.
* Allmän info om CPT 1A på [Wikipedia](https://en.wikipedia.org/wiki/Carnitine_palmitoyltransferase_I_deficiency), [Orphanet](http://www.orpha.net/consor4.01/www/cgi-bin/Disease_Search.php?lng=EN&data_id=1215&title=Carnitine-palmitoyl-transferase-1A-deficiency--CPT1A-deficiency-).
* [Att känna igen en metabolisk kris](https://link.springer.com/chapter/10.1007%2F978-0-85729-923-9_40#page-1) (sida 1-2 i förhandsgranskningen innehåller en lista med symptom). Av Dr Paul J Bellino, ur boken _Pediatric Critical Care Study Guide_ (2012), Springer London.
* [Brev från Emils läkare på Island](sources/CPT1A-letter.pdf) med info om CPT 1A, 11 maj 2013. (På engelska.)
* [Vittnesmål från Emils specialist](sources/T3338-14-vittnesmal-lakare.pdf) ang hans sjukdom, från huvudförhandling i mål T3338-14, Skaraborgs tingsrätt. Utdrag ur protokollet.
* [Kostråd till Emils föräldrar från dietist vid DSBUS](sources/DSBUS-150522.pdf). Bland annat anges hur ofta Emil bör äta. 22 maj 2015.
* [Brev till Emils föräldrar från teamet på DSBUS](sources/DSBUS-140521.pdf), 21 maj 2014. Framgår bland annat att Emil skulle må bra av att börja på förskola.

De flesta tvister mellan Magðalena och Peter efter deras separation kan lite grovt sammanfattas med _"Peter vill träffa Emil mer; Magðalena anser att Peter inte förstår/tar hänsyn till Emils sjukdom/grundläggande behov, och motsätter sig därför umgänge."_ 

* [Brev från Magðalena till Skaraborgs tingsrätt](sources/T3338-14-Lenabrev.pdf) ang hennes oro kring socialtjänstens utredning, 27 augusti 2015
* [Utdrag ur socialtjänstens utredning ang vårdnad, boende och umgänge](sources/Utdrag-Soc.pdf) som lämnades in 20 augusti 2015. (Framgår bl.a. att Magðalena tror att Emil ska ta skada av att vara hos Peter. Filen innehåller utdrag ur flera utredningar.)

Det finns ingen bas för Magðalenas inställning &ndash; faktum är att det finns massor av bevis för att Peter _har_ insikt i hur Emils sjukdom fungerar och hur man tar hand om honom på bästa sätt &ndash; men Magðalenas oro har ändå påverkat myndigheterna eftersom de velat ta det säkra före det osäkra. Du kan läsa mer om hur Emils sjukdom påverkar honom själv och konflikten [på den här sidan](#!cpt1a).

### Alla domstolsdokument {.cyan}

Domstol är Skaraborgs tingsrätt om inget annat anges. Personnummer och vissa andra irrelevanta personuppgifter bortredigerade i de länkade dokumenten. Protokoll som inte länkas här går att begära ut direkt från domstolarna genom att ange målnumret. Många andra handlingar (brev, bilagor mm) i målen, inkl sådana som inte listas här, går också att begära ut med stöd av offentlighetsprincipen.

* [B1093-16, omhäktningsförhandling](sources/B1093-16-omhaktning.pdf), 8 juni 2017
* [T3338-14, andra vårdnadsmålet; huvudförhandling](sources/Protokoll-domstol-20160511.pdf), 11 maj 2016
* T3338-14, andra vårdnadsmålet; förhandling i parternas utevaro 12 april 2016
* [B1093-16, häktningsförhandling](sources/B1093-16.pdf), 31 mars 2016
* [Medlare i T3337-14 begär anstånd för att hitta Magðalena](sources/T3338-14-medlare.pdf) (brev daterat 7 januari 2016)
* T3338-14, andra vårdnadsmålet; förhandling i parternas utevaro 8 december 2015
* T3338-14, andra vårdnadsmålet; förhandling den 26 november 2015
* T3338-14, andra vårdnadsmålet; förberedande förhandling 16 december 2014
* T428-13, första vårdnadsmålet; huvudförhandling 12 december 2013
* Ä1949-13, verkställighet av interimbeslut i T428-13; huvudförhandling 11 juni 2013
* Ö1564-13, avslag på Magðalenas överklagan av det interimistiska beslutet i T428-13. **Göta hovrätt.**
* T428-13, första vårdnadsmålet; förberedande förhandling 14 maj 2013

### Socialtjänsten {.yellow}

Det som följer är **utdrag** endast. Utredningar hos socialtjänsten är belagda med sekretess, och innehåller givetvis väldigt mycket personlig information. Därför tas bara relevanta delar med här. [Kontakta oss](#!z-about) (Peter med sambo) om du vill veta mer, så kan vi avgöra vad vi vill dela med oss av.

Utredningarna gjordes av Socialnämnden/familjerätten i Falköpings kommun [och finns att läsa i denna fil](sources/Utdrag-Soc.pdf). Filen innehåller:

* Utdrag ur utredning om vårdnad, boende och umgänge på uppdrag av Skaraborgs tingsrätt, 20 augusti 2015
* Utdrag ur orosanmälan från Skaraborgs sjukhus i Skövde, 3 september 2014
* Utdrag ur utredning på grund av orosanmälan från anonym person. Avslutad 3 juli 2014

### Övrigt 

* **Magðalenas adressändring.** I protokollen från [huvudförhandlingen i mål T3338-14](sources/Protokoll-domstol-20160511.pdf) och [häktningsförhandlingen i mål B1093-16](sources/B1093-16.pdf) framgår att Magðalena meddelat Skatteverket en isländsk adress. (Informationen kan även hittas på sajter som möjliggör sökning i folkbokföringen; till exempel [upplysning.se](http://www.upplysning.se), och isländska motsvarigheter, som [ja.is](http://ja.is).)