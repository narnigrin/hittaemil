<header><h2 class="violet">I media</h2></header>

Här är en lista på mediebevakningen kring Emils försvinnande, i kronologisk ordning.

--------------------

Har du sett artiklar eller reportage om fallet som inte finns med här? [Tipsa gärna oss](#!x-about)!

### 2016 {.orange}

**31 mars:** Falköpings tidning, ![(svenska)](img/svflag.png){.presslist-language} [_Mamma häktad i vårdnadstvist_](http://www.falkopingstidning.se/article/mamma-haktad-i-vardnadstvist/). Notis om häktningsbeslutet och efterlysningen.

**17 november:** Skaraborgs Allehanda (SLA), ![(svenska)](img/svflag.png){.presslist-language}  [_Kvinna misstänkt &ndash; förde barn utomlands_](http://sla.se/skaraborg/2016/11/17/kvinna-misstankt-forde-barn). Kort sammanfattning av fallet.

### 2017 {.orange}

**19 april:** Fréttablaðið, ![(isländska)](img/isflag.png){.presslist-language} [_Leitar barns og móður á Íslandi með aðstoð spæjara_](http://www.visir.is/g/2017170429974). Intervju med Emils bonusmamma Erika, medan hon är på Island för att leta efter Emil.

**20 april:** Stöð 2's kvällsnyheter, ![(isländska)](img/isflag.png){.presslist-language} [_Leit að íslenskum dreng: "Viljum fá að vita að það sé í lagi með hann"_](http://www.visir.is/g/2017170429905). TV-sändning om fallet och Erikas resa till Island. Reportern intervjuar Erika och man visar upp addressen till denna sajt.

**9 maj:** Fréttablaðið, ![(isländska)](img/isflag.png){.presslist-language} [_Íslenski drengurinn enn týndur og ættingjarnir vilja ekki tala_](http://www.visir.is/g/2017170508929/islenski-drengurinn-enn-tyndur-og-aettingjarnir-vilja-ekki-tala-). Uppföljning på artikeln i april, efter att man talat igen med Erika och Reykjavíkpolisen samt försökt, utan framgång, att kontakta några släktingar till Magðalena.

**1 juni:** Falköpings tidning, ![(svenska)](img/svflag.png){.presslist-language} [_Peters son försvann med sin mamma &ndash; för över ett år sedan: "Det är jättejobbigt varje dag"_](https://www.falkopingstidning.se/article/peters-son-forsvann-med-sin-mamma/). Långt reportage med Peter och Erika, även publicerat i [Skaraborgs läns tidning](https://www.skaraborgslanstidning.se/article/peters-son-forsvann-med-sin-mamma/) och [Västgöta-bladet](https://www.vastgotabladet.se/article/peters-son-forsvann-med-sin-mamma/) och förstasidesnyhet i Skövde nyheter. Man talade också med åklagare för en [relaterad artikel](https://www.falkopingstidning.se/article/svarlost-brott-nar-ena-foraldern-gommer-barn/).