<header><h2 class="green">Vanliga frågor</h2></header>

<div class="preview" markdown="1">
* Är det här en vårdnadstvist?
* Går det här att lita på?
* Är polisen inblandad?
* Varför åkte Magðalena iväg med Emil?
* Var är de någonstans?
* Har man kontrollerat folkbokföringen/Þjóðskrá/andra officiella register?
* Kommer Emil få det bättre om han hittas?
* Kommer Emil tappa kontakten med sin mamma om han hittas?
* Vad är syftet med den här hemsidan, och vad hoppas Peter med familj kunna uppnå med den?
* Vilka är det som står bakom sidan?
</div>

------

### Är det här en vårdnadstvist? {.green}
**Nej.** Att föra bort sitt barn som Magðalena har gjort är ett brott som kan ge fängelse. Magðalena är [häktad i sin frånvaro](sources/B1093-16.pdf) för [grov egenmäktighet med barn](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/brottsbalk-1962700_sfs-1962-700#K7P4) och efterlyst internationellt. Det _har_ funnits en vårdnadstvist, men den var en helt separat process som avslutades, efter flera utredningar, läkarutlåtanden och lång tid, med att pappa Peter fick ensam vårdnad. 

Framför allt innebär detta att sökandet efter Emil inte är någon slags "tvist" mellan föräldrarna, och man behöver inte "välja sida". Alla inblandade myndigheter (polis, åklagare, socialtjänst, Utrikesdepartementet, sjukvården) är överens om att Magðalena och Emil behöver hittas eftersom brottet räknas som att det begås mot Emil (som ju har tvingats skiljas från sin ena förälder), och Emil skulle kunna vara i fara då han kanske inte kan få korrekt läkarvård om han hålls gömd. Bortförande av barn [anses i sig](http://www.childabductions.org/) vara [en slags psykisk misshandel](http://canadiancrc.com/Nancy_Faulkner_Parental_abduction_is_child_abuse_1999.aspx). 

Om Emil hittas har både domstolen, socialtjänsten och pappa Peter sagt att de vill att Magðalena och Emil ska ha en nära kontakt (socialtjänsten föreslog t.ex. umgänge onsdag-söndag varannan vecka).


### Går det här att lita på? {.green}
**Det finns en hel sida med [belägg och dokumentation](#!2-dokument).** Där finns många relevanta dokument att ladda ned direkt, och du kan se målnummer för alla turer i domstolen. Med målnumren kan du själv begära ut handlingar med stöd av offentlighetsprincipen. (Det brukar räcka med att skriva ett mail till domstolen och fråga &ndash; oftast är det avgiftsfritt.) På så vis kan du själv se att det vi berättar är sant.

Vidare så behöver du inte säga något till oss om du vet något om Emil och Magðalena, utan har du information så **ring polisen.** Polisen har all information i fallet och kan avgöra vad som är bäst för Emil; de delar till exempel inte med sig av tips till oss om det inte är lämpligt.

Vill du ha mer information eller dokumentation, är du välkommen att [kontakta Peter och hans sambo](#!x-about). Vissa dokument vill vi inte lägga ut på nätet, men kan tänka oss att visa för enskilda personer.


### Är polisen inblandad? {.green}
**Ja.** Polisen har en förundersökning igång angående [grov egenmäktighet med barn (BrB 7 kap 4 §)](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/brottsbalk-1962700_sfs-1962-700#K7P4). Den 31 mars 2016 begärdes Magðalena [häktad av Skaraborgs tingsrätt, mål B1093-16](sources/B1093-16.pdf). Det finns ett öppet [Haagkonventionsärende](https://www.hcch.net/en/instruments/conventions/full-text/?cid=24) hos Utrikesdepartementet.


### Varför åkte Magðalena iväg med Emil? {.green}
Det är förstås svårt att veta säkert eftersom vi inte kan fråga henne, men med hjälp av vad hon har sagt tidigare kan man göra en kvalificerad gissning.

Magðalena har ända sedan separationen (egentligen ända sedan Emil fick sin sjukdomsdiagnos) haft inställningen att bara hon kan ta hand om Emil. Hon har uttryckligen sagt att Emil skulle vara i fara om Peter tog hand om honom, hon har vägrat låta Emil gå i förskola eftersom hon ansåg att personalen inte kunde sköta om honom och hon har motsatt sig alla Peters och Peters släkts försök att träffa Emil mer. 

Det finns ingen direkt bas för inställningen; faktum är att det finns massor av bevis för motsatsen (till exempel tycker Emils läkare att både Peter och Magðalena är insatta i Emils behov; se fler exempel på [sidan om Emils åkomma](#!cpt1a)), men hon har varit väldigt övertygad, till den grad att hon fokuserat på sjukdomen på bekostnad av allt annat &ndash; hon sade att hennes mål var att få Emil att överleva till vuxen ålder, men verkade enligt socialtjänsten vara oförstående för flera av Emils andra behov.

En möjlighet är att skräcken när Emil fick sin diagnos "spillde över" till en övertygelse att hon ensam visste Emils bästa. Emil hade vid det här laget varit dålig i flera veckor, så man kan lätt förstå att situationen var jobbig för en förälder. Hon har uppgivit att hon blev mycket rädd och trodde att Emil skulle dö, och att hon många gånger sedan dess varit rädd att förlora honom i exempelvis en akut metabolisk kris.

Man kan tänka sig att hon, när hon insåg att hon riskerade förlora vårdnaden, kan ha fått panik och trott att Emil skulle dö och att hon inte hade något val.

Det är värt att påpeka att man kan komma undan lagföring för egenmäktighet med barn (brottet Magðalena står häktad för) om man haft beaktansvärda skäl för handlingen. Dock räcker det inte att man själv _tror_ att man har beaktansvärda skäl, utan det måste också finnas någon rimlig grund för att tro det. _Alla_ som för bort sina barn tror antagligen att de har bra skäl, så den bortförande personens åsikt kan inte i sig fria från brottsmisstanke.


### Var är de någonstans? {.green}
Vi vet inte exakt var Emil och Magðalena befinner sig. Polisen och Utrikesdepartementet antar att hon håller dem gömda på Island, både eftersom hon är uppväxt och har mycket släkt där och eftersom hon i anslutning till försvinnandet postade två "incheckningar" på Facebook i Reykjavíksområdet. Hennes mor, som bodde hos Magðalena i Sverige i två år och hjälpte till att ta hand om Emil, ska även ha sagt att hon "inte behöver åka till Sverige något mera" efter att hon flyttade hem till Island i december 2015. Det är dock inte bekräftat att de är på Island, utan de skulle även kunna vara i USA, Storbritannien, Sverige, Danmark eller något annat land.

Det man vet säkert är att Magðalena inte bor på sin svenska folkbokföringsadress (den lägenheten såldes vid årsskiftet 2015/16), och troligen inte heller på den adress hon angivit som särskild postadress till Skatteverket, eftersom polis inte har kunnat hitta henne där.


### Har man kontrollerat folkbokföringen/Þjóðskrá/andra officiella register? {.green}
**Ja, utan resultat.** Varken på Magðalenas folkbokföringsadress i Falköping eller den adress i Kópavogur, Island, som hon registrerat som särskild postadress (som även är registrerad hos isländska folkbokföringsmyndigheten, Þjóðskrá Íslands), har man kunnat hitta dem. På den svenska adressen bor en orelaterad person (lägenheten såldes strax efter försvinnandet), och den isländska har kontrollerats av både privatdetektiv och polis.

Att isländska register anger att Magðalena är tandläkare är tyvärr inte heller relevant &ndash; hon har tandläkarlegitimation, men det innebär inte med automatik att hon praktiserar sitt yrke.


### Kommer Emil få det bättre om han hittas? {.green}
Vi hoppas det. Peter, hans familj och alla inblandade myndigheter vill att Emil ska få träffa sin mamma mycket när han kommer tillbaka. Det innebär att medan han nu bara får träffa halva sin släkt, och helt berövas kontakt med sin ena förälder, så vill Peter uppmuntra honom att ha tät kontakt med båda sina föräldrar och hela sin släkt, så långt det alls är möjligt.

Han kommer också kunna få tillgång till bättre läkarvård och uppföljning än han har nu, när han lever gömd. Som gömd kan han inte gå på kontroller hos sina specialister, vilket behövs för att veta att han växer och utvecklas ordentligt. Det torde också vara svårt att ge honom nödvändig akutvård (vilket han behöver ganska ofta på grund av sin åkomma) samtidigt som hans hemvist hålls hemlig, och han har ingen dietist som kan ge uppdaterade rekommendationer om hans kost när han växer, så han kan råka utsättas för svältrisk om inte Magðalena gissar rätt på t.ex. hur många kalorier han behöver.

Slutligen riskerar Emil just nu att inte kunna gå i skolan, åtminstone inte under sitt riktiga namn. Om han kommer hem igen kan han börja i skolan och träffa andra barn &ndash; faktum är att kommunen redan har berett honom plats i förskoleklass, för den händelse att han återvänder i tid.


### Kommer Emil tappa kontakten med sin mamma om han hittas? {.green}
**Inte om Peter får bestämma!** Både domstolen, socialtjänsten och Peter själv har sagt att de vill att Magðalena och Emil ska ha en nära kontakt med varandra och att Emil behöver kunna träffa hela sin släkt då och då, inklusive släkten på Island. Socialtjänsten föreslog till exempel umgänge onsdag&ndash;söndag varannan vecka. [Dock kunde inte domstolen besluta](sources/Protokoll-domstol-20160511.pdf) om umgänge när detta senast diskuterades, eftersom Magðalena då befann sig på okänd ort. 

En möjlighet om Magðalena och Emil hittas är att de får ha umgänge med umgängesstöd en period, för att hindra att Emil förs bort igen. Om Magðalena skulle bosätta sig långt bort från Peter, skulle hon och Emil kunna ha kontakt via t.ex. videosamtal under perioder då de inte kan besöka varandra.

Hur som helst är Peter helt inställd på att Emil behöver ha kontakt med båda sina föräldrar. Den enda som hittills försökt förhindra detta är Magðalena.


### Vad är syftet med den här hemsidan, och vad hoppas Peter med familj kunna uppnå med den? {.green}
Först och främst vill vi förstås hitta Emil, och tror att ju fler som känner till att han är försvunnen, desto större är chansen att någon får syn på honom.

Sedan förstår vi att det är svårt att acceptera att en förälder kan göra en sådan här sak. Det kan vara lätt att tänka "ingen rök utan eld" eller undra vad det är som inte berättas. Därför vill vi lägga fram så mycket information vi bara kan, så att vem som helst kan kontrollera själv att det vi säger är sant, och vi vill vara öppna med vad som pågår och visa att vi inte har något att dölja, utan bara vill ha hjälp att hitta Emil.

Allt som vi, Peter och hans sambo, vill är att ha en fungerande familj. Vi vill ge Emil en bra uppväxt där han slipper gömma sig, där han får träffa båda sina föräldrar, gå i skolan som andra barn och får den bästa behandlingen han kan få för sin åkomma. Vi vill inte vänta och undra i resten av våra liv om Emil fortfarande finns och vad som blivit av honom, och vi vill inte att han ska behöva undra vad som hände med oss eller tro att hans pappa har övergivit honom. Han förtjänar mycket bättre än så.


### Vilka är det som står bakom sidan? {.green}
Peter Bengtsson, Emils pappa och enda vårdnadshavare, samt hans sambo Erika. Du kan läsa mer på sidan [om oss](#!x-about).