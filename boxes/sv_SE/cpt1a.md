<header><h2>Emils åkomma, CPT 1A</h2></header>

Emil har en ovanlig kronisk åkomma, CPT 1A. De flesta tvister efter Magðalenas och Peters separation kan grovt sammanfattas med _"Peter vill träffa Emil mer; Magðalena tycker att Peter inte förstår Emils sjukdom, och motsätter sig umgänge."_ 

Socialtjänst och domstol var länge försiktiga med umgänget mellan Peter och Emil eftersom Magðalena inte litade på Peters förmåga. Det finns ingen uppenbar grund för hennes oro (faktiskt finns belägg för motsatsen), men på grund av hur farligt det _skulle ha_ varit för Emil _om_ det hon sade var rätt, tog myndigheterna det säkra före det osäkra och lyssnade på henne.

-----------

### Om CPT 1A
CPT 1A, eller Carnitine palmitoyltransferase typ 1A-brist, är en sällsynt (ca 50 fall i världen) metabolisk sjukdom som i korthet går ut på att kroppen inte kan smälta långa fettsyror, vilket bland annat innebär att man har svårt att tillgodogöra sig energin från fett i maten och att man inte bryter ned fettet som lagrats i kroppens vävnader (vilket låter oss andra klara oss på fastande mage). Behandlingen är dels fettreducerad specialkost, dels regelbundna måltider, så att kroppen inte får slut på energi och hamnar i en metabolisk kris (akut svältsituation). Små barn med CPT 1A behöver även få mat på nätterna, så därför har Emil länge haft en slang (sond) i näsan, genom vilken man kan ge honom mat i vätskeform när han sover eller om han inte äter tillräckligt själv. 

Maten Emil äter ska ha max 20 energiprocent fett, vilket betyder att max 20% av _energin_ (inte vikten eller volymen) i maten får komma från vanligt fett. Man brukar tillsätta en "specialolja" med kortare fettsyror (MCT) som tas upp på ett annat sätt i kroppen, och som därför fungerar för Emil. I praktiken kan han äta de flesta grönsaker, vissa sorters kött, lättmjölk, pasta och gryn med mera utan problem, och det mesta annat i begränsade mängder. Det handlar inte om att helt undvika vissa födomänen, utan att hålla nere det totala fettinnehållet i varje måltid.

Det är också viktigt att Emil åker till sjukhus om han får hög feber eller inte kan hålla nere maten, och han behöver undvika vissa läkemedel. Han gick i Sverige på regelbundna kontroller hos specialister på Drotting Silvias barn- och ungdomssjukhus (DSBUS), och när han besökte andra sjukhus skickade de oftast journalkopior till DSBUS. Han hade också kontakt med en barnläkare på Island, som i sin tur hade kontakt med hans svenska läkare.

Enligt specialisterna är Emils åkomma inget hinder för att gå i förskola eller skola som andra barn, så länge personalen får instruktion av läkarna i vad och när han bör äta och vet vad de ska göra om han blir dålig. Läkarna har till och med rekommenderat att han börjar i förskola.

### Magðalenas farhågor
Magðalena har alltså länge hävdat att Peter inte vet hur man tar hand om Emil, och att Emil skulle ta skada om han var hos Peter. Faktum är att hon har agerat som om det bara är hon (och hennes mor) som kan sköta om Emil överhuvudtaget; Peters mor (som var läkare) och Emils förskola tyckte hon båda var olämpliga, till exempel. Släktingar till Magðalena har till och med sagt till polisen att om Emil inte skulle vara hos Magðalena, skulle han dö.

Att CPT 1A är så ovanlig har troligen bidragit till myndigheternas försiktighet; det finns ganska få människor som har kunskap om åkomman och de flesta som handlagt Emils fall hade troligen inte hört talas om den innan de mötte Emil.

Det omedelbara motargumentet är att Emil i två års tid (dec 2013&mdash;okt 2015) var hos Peter 8 timmar om dagen, 2-3 gånger i veckan; samtidigt skrev hans läkare att han måste få mat var fjärde (senare var femte) timme för att inte ta skada. Vid alla läkarkontroller följde Emil sina tillväxtkurvor och utvecklades i övrigt bra, vilket tyder på att han fått sina grundläggande behov tillgodosedda hos båda föräldrarna. Förhör med Emils specialist i tingsrätten indikerade dessutom att Peter har bra insikt i Emils behov &ndash; läkaren såg inga som helst problem med Peters kunskaper. (Peters vittnesmål är dessutom väldigt likt läkarens ang CPT 1A, trots att läkaren förhördes _efter_ Peter.)

Magðalena anförde också att Emil ofta hade hög temperatur och var trött när han kom tillbaka från Peter. Trötthet _kan_ vara ett symptom på att han inte fått tillräckligt med mat, men det kan också vara att symptom på att han helt enkelt haft mycket för sig (barn som går i förskolan är ofta trötta när de kommer hem, till exempel; och återigen, om han regelbundet fick för lite mat skulle han definitivt inte ha utvecklats normalt). Hög temperatur är däremot inte ett typiskt symptom på metabolisk kris, och de flesta gånger som Magðalena åkt till sjukhus med Emil direkt efter umgänge, har sjukhuset inte uppmätt någon onormal temperatur. Och vanlig feber går som bekant inte att skylla på en förälder.

Under 2015 pekade socialtjänstens utredare på faror med att föräldrarna inte kommunicerade om Emils hälsotillstånd, när han senast hade ätit, med mera. Man fann att Magðalena till och med verkade ha försökt aktivt undanhålla Emils hälsotillstånd från Peter (genom att åka till ett annat sjukhus, inte meddela sig ang. sjukdomar och sjukhusbesök, och rent av försöka hindra Peter från att själv ta med Emil till sjukhuset). Det har dock aldrig varit någon som tvivlat på Magðalenas förmåga att tillgodose Emils grundläggande medicinska behov _när han är hos henne._

### Dokument och mer info {.orange}

##### Om sjukdomen och behandlingen:
* (Del av) [akutbrev från Emils läkare](sources/akutblad-s1.pdf) vid Drottning Silvias barn- och ungdomssjukhus (DSBUS) med information om hans åkomma CPT 1A.
* Allmän info om CPT 1A på [Wikipedia](https://en.wikipedia.org/wiki/Carnitine_palmitoyltransferase_I_deficiency), [Orphanet](http://www.orpha.net/consor4.01/www/cgi-bin/Disease_Search.php?lng=EN&data_id=1215&title=Carnitine-palmitoyl-transferase-1A-deficiency--CPT1A-deficiency-).
* [Att känna igen en metabolisk kris](https://link.springer.com/chapter/10.1007%2F978-0-85729-923-9_40#page-1) (sida 1-2 i förhandsgranskningen innehåller en lista med vanliga symptom). Av Dr Paul J Bellino, ur boken _Pediatric Critical Care Study Guide_ (2012), Springer London.
* [Brev från Emils läkare på Island](sources/CPT1A-letter.pdf) med info om CPT 1A, 11 maj 2013. (På engelska.)
* [Kostråd till Emils föräldrar från dietist vid DSBUS](sources/DSBUS-150522.pdf). Bland annat anges hur ofta Emil bör äta. 22 maj 2015.
* [Brev till Emils föräldrar från teamet på DSBUS](sources/DSBUS-140521.pdf), 21 maj 2014. Framgår bland annat att Emil skulle må bra av att börja på förskola.

##### Om Magðalenas inställning:
* [Brev från Magðalena till Skaraborgs tingsrätt](sources/T3338-14-Lenabrev.pdf) ang hennes oro kring socialtjänstens utredning, 27 augusti 2015
* [Utdrag ur socialtjänstens utredning ang vårdnad, boende och umgänge](sources/Utdrag-Soc.pdf) som lämnades in 20 augusti 2015. (Framgår bl.a. att Magðalena tror att Emil ska ta skada av att vara hos Peter. Filen innehåller utdrag ur flera utredningar.)

##### Tingsrättsförhandlingen och läkares vittnesmål:
* [Protokoll från huvudförhandling i mål T3338-14](sources/Protokoll-domstol-20160511.pdf), Skaraborgs tingsrätt. Personnummer och vissa andra personuppgifter bortredigerade. 