<header><h2>Tidslinje</h2></header>

Det är svårt att hålla reda på allt som händer när relationen mellan två föräldrar går så dåligt att den ena bestämmer sig för att gömma barnet. 

Här följer en överblick i någorlunda kronologisk ordning av de tråkiga händelser som ledde fram till Emils försvinnande.

----------------

(<span class="timeline-small-hide">Du kan skrolla höger och vänster på tidslinjen genom att använda skrollhjulet eller de stora pilarna. </span>Om punkterna på linjen är tätt ihop kan du navigera bland dem med framåt- och bakåt-knapparna i "pratbubblorna.")

<!--[timeline]-->
