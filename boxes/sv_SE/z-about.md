<header><h2 class="cyan">Om oss</h2></header>

![Peter](img/bild4.png) {style=width:49%;max-width:200px;float:left}
![Erika](img/bild1.png) {style=width:49%;max-width:200px;margin-left:2%}

Vi som står bakom den här sajten är Peter, Emils pappa, och Erika, Peters sambo. 

-------

Vi bor i en lägenhet i Falköping och jobbar som programmerare respektive webbutvecklare. Vi träffades 2013, när Emil var 2&hairsp;½ år. Innan han försvann hade vi precis flyttat från en trea till en femma, så att Emil kunde få eget rum. (Han tyckte eget rum var jättespännande och fick välja taklampa och gardiner helt själv.)

För Peter har de senaste åren varit väldigt stressande &ndash; först fick han knappt träffa Emil efter separationen, sedan flera år med socialtjänst och domstol, och till sist när det såg ut som att det skulle bli bättre så blev Emil bortförd av sin mor. Erika har försökt hålla sig vid sidan av, men tog försvinnandet väldigt hårt och tvingades deltidssjukskriva sig för att orka med.

Sedan Emil försvann har våra liv förstås kretsat mycket kring sökandet efter honom. Vi saknar honom varje dag, och undrar hur han har det och om han tänker på oss. Att bli berövad sitt barn är inte någonting som går över, utan det fortsätter att göra ont hela tiden &ndash; frustrationen och maktlösheten när vi inte ens vet var han är går knappt att beskriva. Det är en balansgång att göra allt vi kan för att hitta honom och samtidigt leva våra liv &ndash; världen pausar inte hur jobbigt man än har det, utan man måste fortsätta fungera.

### Kontakta oss

Om du undrar något som du tror vi kan svara på är du välkommen att skriva till oss. Tips om var Magðalena och Emil är ska du i första hand ge **polisen,** men vill du även tipsa oss så går det förstås bra.

<form method="post">
<p style="margin-top:1.3em"><input type="text" name="sbj" style="font-size:1em" placeholder="Ämne/rubrik"></p>
<p><input type="email" name="contact_adr" placeholder="Din e-post *" required></p>
<p><textarea name="contact_form_msg" rows="10" placeholder="Skriv ditt meddelande här. *" required></textarea></p>
<p><button type="submit">Skicka</button></p>
</form>