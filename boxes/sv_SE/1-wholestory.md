<!-- [cols=2] -->
<header><h2 class="blue">Hela historien</h2></header>

**Om du vet något om var Emil och Magðalena finns, ring polisen, tel 114 14.**

Emil Petersson bodde fram till 2015 i Falköping. Föräldrarna hade gemensam vårdnad om honom och han var folkbokförd hos sin mor, Magðalena Níelsdóttir, men hade umgänge med sin pappa flera gånger i veckan. Den 26 november 2015 var föräldrarna kallade till förhandling om vårdnaden i Skaraborgs tingsrätt. Magðalena uteblev utan förklaring och inte ens hennes advokat kunde få tag på henne. Sedan dess har varken myndigheter eller Emils svenska släkt fått kontakt med någon av dem, och Magðalena är häktad i sin frånvaro för brottet grov egenmäktighet med barn. 

---------------

### Försvinnandet
Det var alltså vid vårdnadsförhandlingen den 26 november som det kom fram att Magðalena hade försvunnit med Emil, men redan tidigare fanns tecken på att hon planerade att avvika med honom. Den 14 november klockan 00:20 mailade Magðalena till Peter Bengtsson, Emils far, och meddelade att hon inte tänkte lämna över Emil för umgänge senare den dagen. Hon skrev att hon tänkte "ta semester" med Emil i tre veckor. Magðalena skrev vidare att Peters åsikt i frågan inte var relevant. 

Sedan denna mailkonversation har Peter och Emil inte fått träffas. Magðalena och Emil sågs till i Magðalenas lägenhet mellan den 14:e och den 25:e, men efter den 25:e var de försvunna från lägenheten. Den sista kontakten som Peter och Magðalena hade var den 17 november, då Magðalena via SMS anklagade Peter för att ha undanhållit från henne en kallelse till ögonspecialist som Emil fått.

I december 2015 sålde Magðalenas mor hennes svenska lägenhet med fullmakt. I januari framkom att Magðalena hade sagt upp sig från sitt jobb på Folktandvården Södra Ryd i Skövde och att hon angivit sin mors adress i Kópavogur som s.k. särskild postadress. På hennes Facebookkonto hittades två "incheckningar" på olika platser i Reykjavíksområdet, som senare togs bort. Polisen och Utrikesdepartementet tror därför att Magðalena har tagit Emil till Island, men det är även möjligt att de gömmer sig i något annat land, då Magðalena har släkt i bl.a. Danmark, Norge, Storbritannien, USA och Sverige, och förutom isländska och svenska även talar engelska flytande.

### Vårdnaden
Vid en senare förhandling i vårdnadsmålet beslutades att vårdnaden om Emil skulle tillfalla Peter. (Magðalena representerades vid den här förhandlingen av en god man, eftersom domstolen inte hade fått kontakt med henne. Rätten förhörde även det vittne som Magðalena bett att få kalla innan hon försvann.) Socialtjänsten i Falköping hade sedan tidigare rekommenderat att Emil skulle få rätt till ett omfattande umgänge med Magðalena, men eftersom hon befann sig på okänd ort kunde inte rätten besluta om något umgänge. Dock betyder förstås detta att Magðalena, om hon i framtiden skulle begära umgänge med Emil, skulle ha både domstolens och socialtjänstens stöd. Peter har också sagt att han tycker Emil behöver träffa sin mor.

### Emils sjukdom
Saker kompliceras något av att Emil har en ovanlig kronisk åkomma, CPT 1A (ca 50 fall i världen). I korta drag går den ut på att han inte kan smälta fettsyror ordentligt, vilket bland annat innebär att han inte tillgodogör sig energin från fett i maten och att han inte bryter ned fettet som lagrats i kroppen när han är hungrig (vilket en normal kropp gör, och vilket gör att vi andra kan klara oss ganska länge utan mat). Behandlingen är dels fettsnål specialkost med tillsats av speciella korta fettsyror, dels regelbundna måltider, så att han inte får slut på energi (vilket skulle vara farligt för honom). Små barn med den här sjukdomen behöver även få mat på nätterna, så därför har Emil länge haft en slang (sond) i näsan, genom vilken man kan ge honom mat i vätskeform när han sover eller om han vägrar äta själv.

Det är dessutom viktigt att Emil åker till sjukhus om han får hög feber eller inte kan hålla nere maten, och han behöver undvika vissa läkemedel. Han gick hemma i Sverige på regelbundna kontroller hos specialister på Drotting Silvias barn- och ungdomssjukhus (DSBUS), och när han besökte andra sjukhus skickade de oftast journalkopior till DSBUS. Han hade också kontakt med en barnläkare på Island, som i sin tur hade kontakt med hans svenska läkare.

Enligt specialisterna är Emils åkomma inget hinder för att gå i förskola eller skola som andra barn, så länge personalen får instruktion av läkarna i vad och när han bör äta och vet hur han ska behandlas om han plötsligt blir dålig.

Trots behovet av läkarvård och regelbundna kontroller, har inget sjukhus eller vårdcentral som polisen eller pappa Peter kontaktat sett Emil sedan november 2015, och Emils specialister i Göteborg har inte hört något om honom. Detta innebär antingen att Magðalena skrivit in honom under falskt namn, att hon gått till någon inrättning som ännu inte har tillfrågats, eller att Emil helt enkelt inte fått någon sjukhusvård &ndash; vilket skulle kunna vara livsfarligt för honom.

**Trots detta,** och trots att de varit borta länge, finns det tecken på att Emil fortfarande är vid liv och att det finns folk som vet var de är och hjälper Magðalena. Vi vet dock inte vilka personer som är inblandade, eller vilka som handlar i god tro. (Ingen släkting är i nuläget misstänkt för brott.)

#### Obs!
Magðalena är fortfarande skriven på sin gamla adress i Sverige. (Skatteverket och isländska folkbokföringsmyndigheten har utrett hennes folkbokföring, men adressen står alltså kvar.) Lägenheten ifråga är dock **såld** och de som bor där nu har ingenting med detta att göra!

