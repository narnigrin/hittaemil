<!--[cols=2]-->
<header><h2>Facebookefterlysning</h2></header>

<div class="fullcontent" markdown="1">
Dela gärna våra efterlysningar på Facebook. Efterlysningen finns på [svenska](https://www.facebook.com/photo.php?fbid=10211378569198975&set=a.1177582318751.27852.1203914903) och [engelska](https://www.facebook.com/photo.php?fbid=10211378627600435&set=a.1177582318751.27852.1203914903) samt som kortare sammanfattning på [isländska](https://www.facebook.com/photo.php?fbid=10212659724267051&set=a.1177582318751.27852.1203914903). Tänk på att välja en bra sekretessinställning för din delning, så att posten kan ses av så många eller få som du vill.

Många är av goda skäl skeptiska till efterlysningar på Facebook, men du kan alltid dirigera folk till den här sajten för mer information om de frågar.
</div>

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fnarnigrin%2Fposts%2F10211378611840041&width=auto&show_text=true&height=565&appId" width="auto" height="565" style="border:none;overflow:hidden;width:100%;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>