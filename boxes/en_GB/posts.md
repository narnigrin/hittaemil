<!--[cols=2]-->
<header><h2>Facebook posts</h2></header>

<div class="fullcontent" markdown="1">
Feel free to share our posts about Emil on Facebook. The notice exists in [Swedish](https://www.facebook.com/photo.php?fbid=10211378569198975&set=a.1177582318751.27852.1203914903) and [English](https://www.facebook.com/photo.php?fbid=10211378627600435&set=a.1177582318751.27852.1203914903) and as a shorter summary in [Icelandic](https://www.facebook.com/photo.php?fbid=10212659724267051&set=a.1177582318751.27852.1203914903). Make sure to select an appropriate privacy setting for your share, so that your post is visible to as many or as few people as you want.

Many people are, for good reason, sceptical of this kind of Facebook post, but you can always direct people to this site for more information, should they ask.
</div>

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fnarnigrin%2Fposts%2F10211378632560559&width=auto&show_text=true&height=565&appId" width="auto" height="565" style="border:none;overflow:hidden;width:100%;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>