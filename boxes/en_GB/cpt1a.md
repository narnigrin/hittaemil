<header><h2>Emil's condition, CPT 1A</h2></header>

Emil suffers from a rare chronic condition, CPT 1A. Most conflicts after Magðalena and Peter's separation can be roughly summarized as _"Peter wants to see Emil more; Magðalena believes Peter doesn't understand Emil's disease and opposes Peter's requests."_

For a long time, Social Services and the court were conservative in granting Peter visitation rights, since Magðalena didn't trust Peter to be competent. There's no obvious basis for her worries (actually there's evidence to the contrary), but because of how dangerous it _would have been_ for Emil _if_ she had been right, authorities went for "safe rather than sorry" and listened to her.

----------------

### About CPT 1A
CPT 1A, or Carnitine palmitoyl transferase deficiency type 1A, is a rare (about 50 cases worldwide) metabolic disease which, in short, means Emil's body doesn't process fatty acids normally, implying, among other things, that he gets very little energy from dietary fat and that he can't break down the fat stored in his tissues when he is hungry (which allows the rest of us to get by for extended periods without eating). Treatment is mainly through a low-fat diet and regular meals, so that he never runs out of energy and ends up in a metabolic crisis (essentially starvation). Small children with CPT 1A also need to be fed during the night, so Emil has had a nasal feeding tube through which he could be fed while sleeping or if he won't eat enough on his own.

In Emil's diet, regular long-chain fat must make up no more than 20% of the _total energy content_ (i.e. not 20% fat by weight or volume, but 20% of the calories). It's recommended to supplement this with special shorter-chain fatty acids (MCT) which are metabolised differently, and which therefore work for Emil. In practice he can eat most vegetables, some meats, low-fat milk, pasta and grains, among other things, without problem, and most other foods in limited amounts. It's not a question of completely avoiding certain foods, but of restricting the total fat content.

It is also crucial that Emil gets medical attention if he has a very high temperature or can't keep his food down, and he needs to avoid some types of medication. Back in Sweden, he would regularly see specialists at Drottning Silvias barn- och ungdomssjukhus (DSBUS), the children's hospital in Gothenburg; and other hospitals he visited would inform and consult with DSBUS. He also had a pediatrician in Iceland, who in turn had contact with his Swedish specialists.

According to his specialists, Emil's condition does not stop him from going to preschool or school like other children, as long as school staff are informed by his doctors about his condition and its treatment. Doctors have even recommended that he goes to preschool.

### Magðalena's concerns
For a long time, Magðalena has claimed that Peter doesn't know how to take care of Emil, and that Emil would be harmed if he was with Peter. In fact, she has acted as if she (and her mother) are the only ones who can take care of Emil at all; Peter's mother (who was a doctor) and Emil's preschool were both, according to her, unfit. Relatives of Magðalena's have even told the police that if Emil wasn't with Magðalena, he would die.

That CPT 1A is so uncommon has probably contributed to the authorities' caution; there are relatively few people who understand the disease and most people who have dealt with Emil's case likely hadn't heard of it before they met Emil.

The immediate counter-argument is that for two years (Dec 2013&mdash;Oct 2015), Emil visited Peter for 8 hours a day, 2-3 times a week; at the same time his doctors wrote that he needed to eat every four (later, every five) hours in order not to take harm. At all routine check-ups, Emil was established to grow and develop well, which indicates that his fundamental needs were met by both of his parents. When questioned in court, Emil's specialist indicated that Peter has a good knowledge about Emil's needs&mdash;the doctor saw no problems at all with Peter's competence. (Peter's testimony also happens to be very similar to the doctor's regarding CPT 1A, even though the doctor was questioned _after_ Peter.)

Magðalena also stated that Emil tended to have a high temperature and be tired when he returned from Peter. Tiredness _could_ be a sign he didn't get enough food, but it could also be a sign that he has simply had an exciting day (preschool children are often tired when they come home, for example; and again, if he regularly got too little food he would certainly not have developed normally). A high temperature, however, is not a typical symtom of metabolic crisis, and most times Magðalena took Emil to the hospital right after visiting Peter, the hospitals found his temperature to be normal. And, of course, a common fever can't be blamed on a parent.

In 2015, Social Services caseworkers pointed out that Emil's parents' lack of communication about his health, when he had last eaten, and so forth, might put him in danger. They found that Magðalena had even tried to actively keep important information secret from Peter (by, for instance, going to a different hospital, not letting him know about illnesses and hospital visits, and straight-out trying to stop Peter from taking Emil to the hospital himself). However, no-one has doubted Magðalena's ability to meet Emil's fundamental needs _when he is with her._ 

### Documents and more info {.orange}

##### On the disease and its treatment:
* [Letter from Emil's Icelandic doctor](sources/CPT1A-letter.pdf), 11 May 2013 (in English), with general information about his condition, CPT 1A.
* General info on CPT 1A on [Wikipedia](https://en.wikipedia.org/wiki/Carnitine_palmitoyltransferase_I_deficiency), [Orphanet](http://www.orpha.net/consor4.01/www/cgi-bin/Disease_Search.php?lng=EN&data_id=1215&title=Carnitine-palmitoyl-transferase-1A-deficiency--CPT1A-deficiency-).
* [Recognising a metabolic crisis](https://link.springer.com/chapter/10.1007%2F978-0-85729-923-9_40#page-1) (page 1-2 of the preview contains a list of common symtoms). By Dr Paul J Bellino, from the book _Pediatric Critical Care Study Guide_ (2012), Springer London.
* [Dietary advice from Emil's dietician](sources/DSBUS-150522.pdf), 22 May 2015 (in Swedish). Specifies how often Emil needs to eat, among other things.
* [Letter to Emil's parents from his specialists](sources/DSBUS-140521.pdf), 21 May 2014 (in Swedish). States that Emil would benefit from going to preschool. 

##### On Magðalena's position:
* [Letter from Magðalena to the district court of Skaraborg](sources/T3338-14-Lenabrev.pdf) about her concerns around the recent Social Services enquiry (in Swedish), 27 August 2015
* [Excerpts from Social Services custody and visitation enquiries](sources/Utdrag-Soc.pdf) (in Swedish). (Make clear that Magðalena believes Emil will be harmed by being with Peter, among other things.)

##### The court hearing and Emil's doctor's testimony:
* [Records from main hearing in case T3338-14](sources/Protokoll-domstol-20160511.pdf), district court of Skaraborg (in Swedish). Some personal information redacted.