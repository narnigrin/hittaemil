<header><h2 class="violet">In the news</h2></header>

This is a list of the media coverage of Emil's disappearance, in chronological order.

--------------------

Have you seen an article or broadcast about the case that isn't listed here? Please [let us know](#!x-about)!

### 2016 {.orange}

**31 March:** Falköpings tidning, ![(Swedish)](img/svflag.png){.presslist-language} [_Mamma häktad i vårdnadstvist_](http://www.falkopingstidning.se/article/mamma-haktad-i-vardnadstvist/). Notice about the issuing of the arrest warrant.

**17 November:** Skaraborgs Allehanda (SLA), ![(Swedish)](img/svflag.png){.presslist-language}  [_Kvinna misstänkt &ndash; förde barn utomlands_](http://sla.se/skaraborg/2016/11/17/kvinna-misstankt-forde-barn). Brief summary of the case.

### 2017 {.orange}

**19 April:** Fréttablaðið, ![(Icelandic)](img/isflag.png){.presslist-language} [_Leitar barns og móður á Íslandi með aðstoð spæjara_](http://www.visir.is/g/2017170429974). Interview with Erika, Emil's stepmother, while she searches for Emil in Iceland.

**20 April:** Stöð 2 evening news, ![(Icelandic)](img/isflag.png){.presslist-language} [_Leit að íslenskum dreng: "Viljum fá að vita að það sé í lagi með hann"_](http://www.visir.is/g/2017170429905). Broadcast about the case and Erika's journey to Iceland. The reporter interviews Erika and the broadcast displays the address to this site.

**9 May:** Fréttablaðið, ![(Icelandic)](img/isflag.png){.presslist-language} [_Íslenski drengurinn enn týndur og ættingjarnir vilja ekki tala_](http://www.visir.is/g/2017170508929/islenski-drengurinn-enn-tyndur-og-aettingjarnir-vilja-ekki-tala-). A follow-up to the article in April, after speaking again to Erika and the Reykjavík police and trying unsuccessfully to contact some of Magðalena's relatives.

**1 June:** Falköpings tidning, ![(Swedish)](img/svflag.png){.presslist-language} [_Peters son försvann med sin mamma &ndash; för över ett år sedan: "Det är jättejobbigt varje dag"_](https://www.falkopingstidning.se/article/peters-son-forsvann-med-sin-mamma/). Long interview with Peter and Erika, also published in [Skaraborgs läns tidning](https://www.skaraborgslanstidning.se/article/peters-son-forsvann-med-sin-mamma/) and [Västgöta-bladet](https://www.vastgotabladet.se/article/peters-son-forsvann-med-sin-mamma/) and front-page news in Skövde nyheter. Reporters also spoke to the prosecutor for a [related article](https://www.falkopingstidning.se/article/svarlost-brott-nar-ena-foraldern-gommer-barn/).