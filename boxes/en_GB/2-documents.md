<!--[cols=2]-->
<header><h2 class="red">Supporting documents and evidence</h2></header>

Are you sceptical? **Good!** Critical thinking is not only reasonable but necessary.

Here's a collection of most relevant documents and links to support our story. Should you like to control the veracity of documents yourself, you could, for instance, order court files straight from the court. Contact details for the district court of Skaraborg may be found at [skaraborgstingsratt.domstol.se](http://www.skaraborgstingsratt.domstol.se/). Some other information can also be found in open directories, due to Sweden's principle of public access.

-------------------------------

**Please note** that many of the following documents are in Swedish, as is of course hard to avoid with a legal and criminal case investigated in Sweden. We don't have any formal translations of the documents, but you're welcome to ask us for help with translations or if you need text versions to plug into, say, Google Translate.

### Arrest warrant {.violet}

**On 31 March 2016, a warrant was issued for Magðalena Níelsdóttir's arrest on suspicion of aggravated arbitrary handling of a child.**

* [Records from detainment hearing](sources/B1093-16.pdf) in the district court of Skaraborg, case B1093-16 (in Swedish). Some personal information redacted.
* [Hearing re. continuation of detainment, 8 June 2017](sources/B1093-16-omhaktning.pdf) (case B1093-16; in Swedish). Some personal information redacted.

The crime classification "arbitrary handling of a child" ("egenmäktighet med barn" in Swedish) is defined in [Chapter 7, paragraph 4 of the Swedish Criminal Code](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/brottsbalk-1962700_sfs-1962-700#K7P4) (link in Swedish) and is applicable when someone who does not have _sole custody_ of a child separates that child, without good reason, from one or both of its custodians without a custodian's permission, or alternatively keeps the child with them without a custodian's permission. It can be committed by a person who does not have custody of the child, or one who has joint custody; and the person may have had permission to take the child from the (other) custodian but kept it with them for too long, for example by refusing court-ordered visitation. The crime is considered aggravated if, for instance, the child is taken or held abroad, if the child is held for a long time, or if the abductor appears to plan not to return. Aggravated arbitrary handling of a child can lead to prison from 6 months to 4 years.

### Custody {.green}

**Custody of Emil Petersson was, on 11 May 2016, granted solely to Peter Bengtsson, his father.** The ruling was backed by Social Services. Before the ruling, Emil's parents had joint custody and Emil lived with his mother, Magðalena Níelsdóttir.

* [Records from main hearing in case T3338-14](sources/Protokoll-domstol-20160511.pdf), district court of Skaraborg (in Swedish). Some personal information redacted.
* [Excerpts from Social Services custody and visitation enquiry](sources/Utdrag-Soc.pdf) submitted to the district court on 20 August 2015 (in Swedish). The PDF includes excerpts from multiple enquiries.

### Emil's condition {.orange}

* (Part of) ["emergency letter" from Emil's specialists](sources/akutblad-s1.pdf); intended to be shown to A&E personnel and includes some information about his condition, CPT 1A. (In medical Swedish.)
* [Letter from Emil's Icelandic doctor](sources/CPT1A-letter.pdf), 11 May 2013 (in English), with general information about CPT 1A.
* General info on CPT 1A on [Wikipedia](https://en.wikipedia.org/wiki/Carnitine_palmitoyltransferase_I_deficiency), [Orphanet](http://www.orpha.net/consor4.01/www/cgi-bin/Disease_Search.php?lng=EN&data_id=1215&title=Carnitine-palmitoyl-transferase-1A-deficiency--CPT1A-deficiency-).
* [Recognising a metabolic crisis](https://link.springer.com/chapter/10.1007%2F978-0-85729-923-9_40#page-1) (page 1-2 of the preview contains a list of common symtoms). By Dr Paul J Bellino, from the book _Pediatric Critical Care Study Guide_ (2012), Springer London.
* [Testimony by Emil's specialist](sources/T3338-14-vittnesmal-lakare.pdf) about his disease, from district court case T3338-14 (in Swedish).
* [Dietary advice from Emil's dietician](sources/DSBUS-150522.pdf), 22 May 2015 (in Swedish). Specifies how often Emil needs to eat, among other things.
* [Letter to Emil's parents from his specialists](sources/DSBUS-140521.pdf), 21 May 2014 (in Swedish). States that Emil would benefit from going to preschool. 

Most conflicts after Magðalena and Peter's separation can be roughly summarized as _"Peter wants to see Emil more; Magðalena believes Peter doesn't understand/respect Emil's disease/fundamental needs, and opposes Peter's requests."_

* [Letter from Magðalena to the district court of Skaraborg](sources/T3338-14-Lenabrev.pdf) about her concerns around the recent Social Services enquiry (in Swedish), 27 August 2015
* [Excerpts from Social Services custody and visitation enquiries](sources/Utdrag-Soc.pdf) (in Swedish). (Make clear that Magðalena believes Emil will be harmed by being with Peter, among other things.)

There is no basis for Magðalena's stance&mdash;in fact there is lots of evidence that Peter does have insight into how Emil's condition works and how to care for him in the best way possible&mdash;but Magðalena's worries have still influenced authorities, as they have understandably preferred to be safe rather than sorry. You can read more about how Emil's condition affects him and the conflict [on this page](#!cpt1a).

### All court documents {.cyan}

Presiding court is the district court of Skaraborg unless otherwise noted. Some personal information has been redacted in the linked documents. Records not linked here may be requested from the courts by mentioning the case number. Many other relevant case files which are not listed here can also be requested due to Sweden's principle of public access.

Obviously, the absolute majority (if not all) of court documents will be in Swedish.

* [B1093-16, hearing re. continuation of detainment](sources/B1093-16-omhaktning.pdf), 8 June 2017
* [T3338-14, second custody case; main hearing](sources/Protokoll-domstol-20160511.pdf), 11 May 2016
* T3338-14, second custody case; hearing in the parties' absence, 12 April 2016
* [B1093-16, detainment hearing](sources/B1093-16.pdf), 31 March 2016
* [Court mediator in T3338-14 requests an extension to locate Magðalena](sources/T3338-14-medlare.pdf) (letter dated 7 January 2016)
* T3338-14, second custody case; hearing in the parties' absence 8 December 2015
* T3338-14, second custody case; hearing on 26 November 2016
* T3338-14, second custody case; preparatory hearing on 16 December 2014
* T428-13, first custody case; main hearing, 12 December 2013
* Ä1949-13, enforcement hearing regarding the interim ruling in T428-13; main hearing on 11 June 2013
* Ö1564-13, refusal of Magðalena's request to appeal the interim ruling in T428-13. **Göta Hovrätt court of appeal.**
* T428-13, first custody case; preparatory hearing, 14 May 2013

### Social Services {.yellow}

What follows is **excerpts** only. Social Services enquiries are confidential, and necessarily contain a large amount of very personal information. Because of this, only relevant portions are included here. [Contact us](#!z-about) (Peter and his partner) if you want to know more, and we can decide how much we want to share.

Enquiries were made by Socialtjänsten (Social Services) in the municipality of Falköping [and are available in this file](sources/Utdrag-Soc.pdf). The file contains:

* Excerpt from the custody and visitation enquiry submitted to the district court of Skaraborg on 20 August 2015
* Excerpt from the report to Social Services from Skaraborg Hospital in Skövde, 3 September 2014
* Excerpt from the enquiry started after an anonymous report, ended 3 July 2014

### Miscellaneous

* **Magðalena's address change.** In the records from the [main hearing in case T3338-14](sources/Protokoll-domstol-20160511.pdf) and the [detainment hearing in case B1093-16](sources/B1093-16.pdf), it is clear that Magðalena has given Swedish authorities an Icelandic address. (Postal addresses in court documents are always taken from the Swedish national registration, and in these cases Magðalena's listed address is Icelandic.) The information can also be found using web sites which allow national registration searches; for example [upplysning.se](http://www.upplysning.se), and Icelandic equivalents, like [ja.is](http://ja.is).