<header><h2 class="green">Frequently asked questions</h2></header>

<div class="preview" markdown="1">
* Is this a custody dispute?
* Is this trustworthy?
* Are the police involved?
* Why did Magðalena run away with Emil?
* Where are they?
* Has anyone checked official address records (folkbokföringen, Þjóðskrá, etc)?
* Will things be better for Emil if he is found?
* Will Emil lose contact with his mother if he is found?
* What's the purpose of this web site, and what does Peter and his family hope it will achieve?
* Who is behind this site?
</div>

------

### Is this a custody dispute? {.green}
**No.** Abducting one's child like Magðalena did is a crime which can lead to a prison sentence. [Magðalena is wanted](sources/B1093-16.pdf) in multiple countries for a crime called aggravated arbitrary handling of a child (["grov egenmäktighet med barn" in Swedish](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/brottsbalk-1962700_sfs-1962-700#K7P4)). There _was_ a custody dispute, but that was a completely separate process which was ended, after a number of enquiries, statements from doctors and court hearings, when Emil's daddy Peter got sole custody.


### Is this trustworthy? {.green}
**There is a whole page of [documentation](#!2-documents).** There you'll find a number of relevant documents for immediate download and case numbers for all relevant court cases. You can use the case numbers to get direct access to the case files from the Swedish courts (anyone&mdash;Swedish and foreign citizens alike&mdash;can request court documents free of charge by simply sending the court an e-mail). In this way, you can see for yourself that what we say is true.

Furthermore, you don't need to tell us anything you may know about Emil or Magðalena&mdash;if you have information please instead **call the police.** The police have all the information about the case and can decide what's best for Emil; for instance, they won't share anything with us unless they think it's warranted.

Should you want any further information or documentation, you're welcome to [contact Peter and his partner](#!x-about). There are documents that we don't want to put online, but that we're okay with sharing privately.

(Note that much of the documentation is in Swedish&mdash;this is of course hard to avoid with a legal and criminal case investigated in Sweden! We don't have any formal translations of the documents, but you're welcome to ask us for help with translations or if you need text versions to plug into, say, Google Translate.)


### Are the police involved? {.green}
**Yes.** Swedish police are investigating Magðalena for aggravated arbitrary handling of a child (["grov egenmäktighet med barn" in Swedish](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/brottsbalk-1962700_sfs-1962-700#K7P4)) and on 31 March 2016, they [issued a warrant for her arrest](sources/B1093-16.pdf). There is an open [Hague Convention](https://www.hcch.net/en/instruments/conventions/full-text/?cid=24) international child abduction case with the Swedish Foreign Office.


### Why did Magðalena run away with Emil? {.green}
This is of course hard to know for sure since we can't ask her, but considering things she said and did before, it's possible to make an educated guess.

Ever since the separation (actually since Emil got his diagnosis), Magðalena has had an attitude to the effect that only she is able to care for Emil. She said explicitly that Emil would be in danger if Peter took care of him, refused to let Emil attend preschool since she believed the staff weren't able to care for him and has opposed all Peter's and Peter's family's attempts to spend more time with Emil.

There is no real basis for her stance; in fact there's solid evidence to the contrary (for example, Emil's doctors believe that both Peter and Magðalena are well-informed about Emil's needs; for more examples see [the page about Emil's condition](#!cpt1a)), but she has been very convinced, to the point that she focused on the disease at the cost of everything else&mdash;she said her goal was for Emil to survive his childhood, but appeared (according to Social Services) not to understand many of his other needs.

One possibility is that her fear when Emil was diagnosed "spilled over" into a belief that she alone understood what was best for him. At this point, Emil had been ill for weeks, so it's easy to understand how the situation was taxing for a parent. She has stated that she was very afraid and thought Emil was going to die, and that many times since then she has been afraid to lose him in, for instance, a sudden metabolic crisis.

Perhaps when she realised she stood to lose custody, she might have panicked and believed that Emil was going to die and that she didn't have a choice.

It's worth noting that it's possible to avoid prosecution for arbitrary handling of a child (the crime of which Magðalena is suspected) if one has notable reason to commit the act. However, it isn't enough to _believe_ oneself to have notable reason, there must also be some reasonable basis for believing so. _Everyone_ who abducts their child probably thinks they have good reasons, so the abductor's opinion can't in itself be a basis to avoid prosecution.


### Where are they? {.green}
We don't know exactly where Emil and Magðalena are. The police and the Swedish Foreign Office work under the assumption that she is keeping them hidden in Iceland, since she grew up and has most of her family there and since she posted two "check-ins" in the Reykjavík area to her Facebook account in connection with their disappearance. Her mother, who lived with Magðalena in Sweden for two years and helped raise Emil, is also reported to have said she "doesn't need to go to Sweden any more" after moving back to Iceland in December 2015. They aren't confirmed to be in Iceland, however, and might also be in the US, the UK, Sweden, Denmark or some other country.

What we know for sure is that Magðalena no longer lives at her officially registered address in Sweden (that flat was sold around January 2016), and probably not at the address she has given to Swedish authorities as a temporary forwarding address, as police have not been able to find her there.


### Has anyone checked official address records (folkbokföringen, Þjóðskrá, etc)? {.green}
**Yes, fruitlessly.** Neither at Magðalena's formal address of residence in Falköping, Sweden (registered with Swedish folkbokföringen) nor at her forwarding address in Kópavogur, Iceland (registered with both Swedish authorities and Iceland's Þjóðskrá) has anyone found them. At the Swedish address lives an unrelated person (the flat was sold soon after the disappearance), and the Icelandic one has been checked by both police and private investigator.

The fact that Icelandic records state Magðalena is a dentist is unfortunately also unhelpful&mdash;she has a degree in dentistry, but that does not mean she practices as one now.


### Will things be better for Emil if he is found? {.green}
We hope so. Peter, his family and all involved authorities want Emil to see his mum frequently when he returns. This means that while he's currently only able to see half his family, and is completely deprived of contact with one of his parents, Peter wants to encourage him to have close contact with both of his parents and all of his relatives, as far as at all possible.

He will also have access to better health care and follow-ups than he has now that he's staying hidden. Living in hiding, he can't have check-ups with his specialists, which he needs to make sure he's growing and developing properly. It should also be difficult to give him necessary emergency care (which he needs quite often because of his condition) while his whereabouts are kept secret, and he doesn't have a dietician who can give updated advice on his diet when he grows, so he could be accidentally exposed to the risk of starvation unless Magðalena makes a good guess on how many calories he needs now, for example.

Finally, Emil currently risks not being able to go to school, at least not under his real name. If he comes home he could start school and meet other children&mdash;in fact, the municipality of Falköping has already reserved a place for him at a local school, in case he comes back in time.


### Will Emil lose contact with his mother if he is found? {.green}
**Not if Peter has any say in the matter!** Both the court, Social Services and Peter himself have said they want Emil and Magðalena to be in close contact and that Emil needs to be able to see all his relatives now and then, including those in Iceland. Social Services even suggested visiting hours every other Wednesday&ndash;Sunday. [The court was unable to judge](sources/Protokoll-domstol-20160511.pdf) on visitation hours when it was last discussed, however, since Magðalena's whereabouts were by then unknown.

One possibility if Magðalena and Emil are found is to use custody support when they meet, to stop Emil from being abducted again. If Magðalena were to move far away from Peter, she and Emil could have contact via video call, for example, during periods when they can't meet in person.

Either way, Peter is convinced that Emil needs contact with both of his parents. The only one who has so far tried to stop that is Magðalena.


### What's the purpose of this web site, and what does Peter and his family hope it will achieve? {.green}
First of all we want to find Emil, of course, and we believe that the more people know that he is missing, the greater the chance someone will spot him.

Secondly, we understand how hard it is to accept that a parent would do something like this. It might be easy to think "where there's smoke, there's fire" or wonder what isn't being said. That's why we want to present as much information as we can, so that anyone can control themselves that what we say is true, and we want to be open with what's going on and show that we have nothing to hide, that we only want to find Emil.

All that we, Peter and his partner, want is to have a normal, functioning family. We want to give Emil a decent childhood where he doesn't have to hide, where he can meet both his parents, go to school like other children and get the best possible treatment for his condition. We don't want to wait and wonder, for the rest of our lives, whether Emil is still alive and what has become of him, and we don't want him to have to wonder what happened to us or believe that his daddy has abandoned him. He deserves so much better.


### Who is behind this site? {.green}
Peter Bengtsson, Emil's father and sole custodian, and his partner Erika. You can read more on the page [about us](#!x-about).