<header><h2 class="cyan">About us</h2></header>

![Peter](img/bild4.png) {style=width:49%;max-width:200px;float:left}
![Erika](img/bild1.png) {style=width:49%;max-width:200px;margin-left:2%}

We are Peter, Emil's daddy, and Erika, Peter's partner, and we're responsible for this site.

-------

We live in a flat in Falköping, Sweden, and work as a programmer and web developer, respectively. We met in 2013, when Emil was 2&hairsp;½ years old. Before he disappeared, we had just moved from a two-bedroom flat into a three-bedroom flat so that Emil could have his own room. (He thought having his own room was very exciting and got to choose his curtains and ceiling lamp himself.)

For Peter, the last few years have been very stressful&mdash;first he barely got to meet Emil at all after the separation, then year after year with Social Services and the court, and finally when things seemed to improve, Emil was abducted by his mother. Erika has tried to stay out of the conflict, but was hit very hard by Emil's disappearance and had to take out sick leave to cope.

Since Emil went missing, our lives have naturally revolved a lot around the search for him. We miss him every day and wonder how he's doing and whether he thinks about us. Being bereft of a child isn't something that just passes; it keeps on hurting all the time&mdash;the frustration and powerlessness of not even knowing where he is is impossible to describe. It's a balance to do everything we can to find him and simultaneously keep living our lives&mdash;the world doesn't stop turning no matter how bad things get; you have to keep functioning anyway.

### Contact us

If you have a question you think we can answer, you're welcome to write to us. Tips about Magðalena's and Emil's whereabouts are best given to the **police,** but if you want to let us know as well you're of course welcome to do so.

<form method="post">
<p style="margin-top:1.3em"><input type="text" name="sbj" style="font-size:1em" placeholder="Subject"></p>
<p><input type="email" name="contact_adr" placeholder="Your e-mail *" required></p>
<p><textarea name="contact_form_msg" rows="10" placeholder="Please type your message. *" required></textarea></p>
<p><button type="submit">Send</button></p>
</form>