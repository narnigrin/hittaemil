<header><h2>Timeline</h2></header>

It's hard to keep track of everything that happens when a relationship between parents breaks so badly that one decides to hide the child from the other.

Here is a more-or-less chronological overview of the events that led up to (and followed) Emil's disappearance.

----------------

(<span class="timeline-small-hide">You can scroll right and left in the timeline using the scroll wheel or the large right/left arrows. </span>Where the markers are clustered close together, you can navigate between them using the forward and back buttons in the "speech bubbles.")

<!--[timeline]-->
