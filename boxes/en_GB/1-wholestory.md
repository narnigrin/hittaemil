<!-- [cols=2] -->
<header><h2 class="blue">The whole story</h2></header>

**If you have any information about where Emil and Magðalena are, contact the police**  
(Swedish police, +46 77 114 14 00, or ask the nearest police station for help).

Emil Petersson lived, until late 2015, in Falköping, Sweden. His parents had joint custody and he lived with his mother, Magðalena Níelsdóttir, but visited his father multiple times each week. On November 26th, 2015, his parents were due in court for a custody hearing. Without explanation, Magðalena failed to appear; not even her lawyer was able to contact her. Since then, neither the authorities nor Emil's Swedish relatives have had any contact with either of them, and Magðalena is wanted internationally for child abduction.

----------------------------

### The disappearance
It was during the hearing on 26 November 2015 that it became evident that Magðalena had disappeared with Emil, but there were signs earlier on that she planned to run away with him. On 14 November at 12:20 a.m., Magðalena e-mailed Peter Bengtsson, Emil's father, to inform him that he would not be able to pick up Emil for a scheduled visit later that day. She wrote that she was "taking a three-week vacation" with Emil starting immediately. Magðalena wrote, further, that Peter's opinion in the matter was irrelevant.

Peter and Emil have not met or spoken since this e-mail exchange. Magðalena and Emil were seen in Magðalenas flat between the 14th and the 25th, but after the 25th, they were gone from the flat. The last contact between Peter and Magðalena was on 17th November when, via text message, she accused him of having kept secret an appointment with Emil's eye specialist.

In December 2015, Magðalena's mother sold Magðalena's Swedish flat via power of attorney. In January it emerged that Magðalena had resigned from her job at the Swedish Dental Service in Skövde and that she had given Swedish authorities her mother's address in Kópavogur as a temporary forwarding address. Two "check-ins" at different locations around Reykjavík appeared on her Facebook timeline, but were later deleted. This has all led the police and the Swedish Foreign Office to assume Magðalena has taken Emil to Iceland, but it is also possible that they are hiding in some other country, as Magðalena has relatives in Denmark, Norway, the UK, the US and Sweden, among other countries, and apart from Icelandic and Swedish also speaks English fluently.

### Custody
At a new custody hearing some time later, it was ruled that Peter would have sole custody of Emil. (Magðalena was represented by a court-appointed administrator, since she hadn't responded to any contact attempts by the court. The witness which Magðalena had asked to summon before she went missing was also questioned.) Social Services had previously recommended that Magðalena get extensive visitation rights, but since her whereabouts were unknown, the court was unable to make any informed judgement on the issue. However, this means that should Magðalena request visitation rights in the future, she would have the support of both the court and Social Services. Peter has also stated that he believes Emil needs to see his mother regularly.

### Emil's disease
Matters are complicated somewhat by the fact that Emil suffers from a rare chronic condition, CPT 1A (about 50 cases worldwide). Briefly speaking, his body doesn't process fatty acids normally, which means, among other things, that he gets very little energy from dietary fat and that he can't break down the fat stored in his tissues when he is hungry (like healthy bodies do, which allows the rest of us to get by for extended periods without eating). Treatment is mainly through a low-fat diet supplemented with short-chain fatty acids, and very regular meals, so that he never runs out of energy (which would be dangerous for him). Small children with CPT 1A also need to be fed during the night, so Emil has had a nasal feeding tube through which he could be fed while sleeping or if he won't eat enough on his own.

It is also crucial that Emil gets medical attention if he has a very high temperature or can't keep his food down, and he needs to avoid some types of medication. Back in Sweden, he would regularly see specialists at Drottning Silvias barn- och ungdomssjukhus (DSBUS), the children's hospital in Gothenburg; and other hospitals he visited would inform and consult with DSBUS. He also had a pediatrician in Iceland, who in turn had contact with his Swedish specialists.

According to his specialists, Emil's condition does not stop him from going to preschool or school like other children, as long as school staff are informed by his doctors about his condition and its treatment. 

Despite the need for regular check-ups and hospital visits, no hospital that the police or his daddy, Peter, have spoken to has seen Emil since November 2015, and Emil's specialists in Gothenburg haven't heard any mention of him. This means that either Magðalena has had him see doctors under a false name, that he has visited an institution not yet approached, or simply that Emil has not seen a doctor at all&mdash;which could be extremely dangerous for him.

**In spite of this,** and although they have been gone for a long time, there are signs Emil is still alive and that there are people helping Magðalena keep him hidden. We don't know who might be involved, however, and who might be acting in good faith. (No relatives are as yet suspected of any crime.)

#### Please note
Magðalena is still formally registered at her old Swedish address. (Swedish and Icelandic national registration authorities have had her under investigation, but the old adress remains so far.) The flat in question has been **sold** and the people who live there now are **completely unrelated!**